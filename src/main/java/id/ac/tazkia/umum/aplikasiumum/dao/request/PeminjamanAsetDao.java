package id.ac.tazkia.umum.aplikasiumum.dao.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanAset;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;

public interface PeminjamanAsetDao extends PagingAndSortingRepository<PeminjamanAset, String> {
    List<PeminjamanAset> findByStatusAndUserOrderByTanggalPengajuan(StatusRecord statusRecord, User user);
    List<PeminjamanAset> findByStatusAndTanggalMulaiPeminjamanBetween(StatusRecord statusRecord, String mulai, String berakhir);
    List<PeminjamanAset> findByStatusOrderByTanggalPengajuanDesc(StatusRecord statusRecord);
    List<PeminjamanAset> findByStatusAndStatusPeminjaman(StatusRecord statusRecord, String statusPeminjaman);
}
