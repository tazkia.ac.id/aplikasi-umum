package id.ac.tazkia.umum.aplikasiumum.dao.request;

import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanAsetApproval;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PeminjamanAsetApprovalDao extends PagingAndSortingRepository<PeminjamanAsetApproval, String> {
    @Query(value = "select b.id, c.nama_karyawan, e.nama_jabatan, a.status, a.tanggal_approval from peminjaman_aset_approval as a\n" +
            "            inner join peminjaman_aset as b on a.id_peminjaman_aset = b.id\n" +
            "            inner join karyawan as c on a.id_karyawan = c.id\n" +
            "            inner join karyawan_jabatan as d on d.id_karyawan = c.id\n" +
            "            inner join jabatan as e on d.id_jabatan = e.id\n" +
            "            where d.status = 'AKTIF' and c.status = 'AKTIF' and b.status = 'AKTIF'\n" +
            "            order by a.nomor asc", nativeQuery = true)
    Object[] listApproval();
}
