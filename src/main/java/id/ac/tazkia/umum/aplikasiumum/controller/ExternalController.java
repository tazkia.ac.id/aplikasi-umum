package id.ac.tazkia.umum.aplikasiumum.controller;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import groovy.util.ResourceException;
import id.ac.tazkia.umum.aplikasiumum.dao.config.RoleDao;
import id.ac.tazkia.umum.aplikasiumum.dao.config.UserDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.*;
import id.ac.tazkia.umum.aplikasiumum.dao.request.*;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.Role;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.*;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanAset;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanRuangan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PerbaikanSapras;
import id.ac.tazkia.umum.aplikasiumum.entity.request.Vendor;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import id.ac.tazkia.umum.aplikasiumum.service.GmailApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class ExternalController {

    @Value("${upload.bukti}")
    private String uploadBukti;

    @Value("classpath:templates/user_manual.pdf")
    private Resource userManual;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PeminjamanAsetDao peminjamanAsetDao;

    @Autowired
    private AsetJenisDao asetJenisDao;

    @Autowired
    private AsetDao asetDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private VendorDao vendorDao;

    @Autowired
    private VendorJenisDao vendorJenisDao;

    @Autowired
    private PengadaanAsetDao pengadaanAsetDao;

    @Autowired
    private PerbaikanSaprasDao perbaikanSaprasDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PeminjamanRuanganDao peminjamanRuanganDao;

    @Autowired
    private PeminjamanKendaraanDao peminjamanKendaraanDao;

    @Autowired
    private RuanganDao ruanganDao;

    @Autowired
    private GmailApiService gmailApiService;

    @Autowired
    private MustacheFactory mustacheFactory;

    @Autowired
    private MobilDao mobilDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private JabatanDao jabatanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @ModelAttribute("kendaraan")
    private Iterable<Mobil> listKendaraan(){
        return mobilDao.findByStatus(StatusRecord.AKTIF);
    }

    @ModelAttribute("ruangan")
    public Iterable<Ruangan> listRoom(){
        return ruanganDao.findByStatusRuanganAndStatus("BISA_DIPINJAMKAN", StatusRecord.AKTIF);
    }

    @ModelAttribute("asetCategory")
    public Iterable<AsetJenis> listAsetCategory(){
        return asetJenisDao.findByStatusAndStatusPeminjaman(StatusRecord.AKTIF, "BISA_DIPINJAM");
    }

    @ModelAttribute("vendorJenis")
    public Iterable<VendorJenis> listVendorJenis(){
        return vendorJenisDao.findByStatus(StatusRecord.AKTIF);
    }

    @GetMapping("/external/form")
    public void form(){

    }

    @PostMapping("/external/form")
    public String isiForm(@RequestParam(required = false) String nama, @RequestParam String email, @RequestParam String noHp, @RequestParam String organisasi){

        return "redirect:/external/pilihAset?nama=" + nama + "&email=" + email + "&noHp=" + noHp + "&organisasi=" + organisasi;
    }

    @GetMapping("/qr")
    public void getBarcode(Model model, @RequestParam String code){
        model.addAttribute("dataBarang", asetDao.findByBarcode(code));
    }

    @GetMapping("/external/pilihAset")
    public void pilihAset(Model model, @RequestParam(required = false) String nama, @RequestParam(required = false) String email,
                          @RequestParam(required = false) String noHp,
                          @RequestParam(required = false) String organisasi,
                          @RequestParam(required = false)String jenisAset, @RequestParam(required = false) String kegiatan,
                          @RequestParam(required = false) String tanggalMulaiPeminjaman,
                          @RequestParam(required = false) String tanggalBerakhirPeminjaman){

        model.addAttribute("nama", nama);
        model.addAttribute("email", email);
        model.addAttribute("noHp", noHp);
        model.addAttribute("organisasi", organisasi);

        if (jenisAset != null){
            model.addAttribute("jenisAset", jenisAset);
            model.addAttribute("kegiatan", kegiatan);
            model.addAttribute("tanggalMulai", tanggalMulaiPeminjaman);
            model.addAttribute("tanggalBerakhir", tanggalBerakhirPeminjaman);

            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            LocalDate mulai = LocalDate.parse(tanggalMulaiPeminjaman);
            LocalDate berakhir = LocalDate.parse(tanggalBerakhirPeminjaman);
            System.out.println("Mulai : " + mulai);
            System.out.println("Berakhir : " + berakhir);

            Period diff = Period.between(mulai, berakhir);
            int hasil = diff.getDays() + 1;

            model.addAttribute("berapaLama", hasil);
            AsetJenis asetJenis = asetJenisDao.findById(jenisAset).get();




            List<PeminjamanAset> peminjamanAsets = peminjamanAsetDao.findByStatusAndTanggalMulaiPeminjamanBetween(StatusRecord.AKTIF, tanggalMulaiPeminjaman, tanggalBerakhirPeminjaman);
            if (peminjamanAsets.isEmpty()){
                model.addAttribute("cek", "kosong");
            }else {
                model.addAttribute("cek", "ada");
                model.addAttribute("cekAset", peminjamanAsetDao.findByStatusAndTanggalMulaiPeminjamanBetween(StatusRecord.AKTIF, tanggalMulaiPeminjaman, tanggalBerakhirPeminjaman));
            }
            model.addAttribute("aset", asetDao.findByStatusAndAsetJenisAndStatusPeminjaman(StatusRecord.AKTIF, asetJenis, "BISA_DIPINJAM"));

        }
    }

//    @PostMapping("/external/pilihAset")
//    public String pilihAsetPost(PeminjamanAset peminjamanAset){
//        LocalDate localDate = LocalDate.now(ZoneId.of("UTC+7"));
//        System.out.println("test : " + localDate);
//        peminjamanAset.setStatus(StatusRecord.AKTIF);
//        peminjamanAset.setTanggalPengajuan(localDate.toString());
//        peminjamanAset.setStatusPeminjaman("WAITING");
//        peminjamanAset.setStatusPembayaran("BELUM_DIBAYAR");
//        peminjamanAsetDao.save(peminjamanAset);
//
//        return "redirect:/external/list?noHp=" + peminjamanAset.getNoHp();
//    }

    @PostMapping("/external/hapus")
    public String hapusAssetLoan(@RequestParam PeminjamanAset peminjamanAset, @RequestParam String noHp){
        peminjamanAset.setStatus(StatusRecord.HAPUS);
        peminjamanAsetDao.save(peminjamanAset);
        return "redirect:/external/list?noHp=" + noHp;
    }

    /*@GetMapping("/external/list")
    public void listLoanAset(Model model, @RequestParam(required = false) String noHp){
        model.addAttribute("noHp", noHp);
        if (noHp != null){
            model.addAttribute("noHp", noHp);
            model.addAttribute("listData", peminjamanAsetDao.findByStatusAndNoHp(StatusRecord.AKTIF, noHp));
        }
    }*/

    @PostMapping("/external/upload")
    public String upload(@Valid PeminjamanAset peminjamanAset, MultipartFile file) throws Exception{
        PeminjamanAset pAset = peminjamanAsetDao.findById(peminjamanAset.getId()).get();
        if (!file.isEmpty() || file != null){
            String namaFile = file.getName();
            String namaAsliFile = file.getOriginalFilename();
            String extension = "";

            int i = namaAsliFile.lastIndexOf('.');
            int p = Math.max(namaAsliFile.lastIndexOf('/'), namaAsliFile.lastIndexOf('\\'));
            if (i > p){
                extension = namaAsliFile.substring(i + 1);
            }

            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadBukti + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
            peminjamanAset.setBuktiPembayaran(idFile + "." + extension);
        }else {
            peminjamanAset.setBuktiPembayaran(peminjamanAset.getBuktiPembayaran());
        }
        peminjamanAset.setTanggalPengajuan(pAset.getTanggalPengajuan());
        peminjamanAset.setStatusPembayaran("SUDAH_DIBAYAR");
        peminjamanAsetDao.save(peminjamanAset);
        return "redirect:/assetLoan/list";
    }

    @GetMapping("/external/vendor/form")
    public void vendorForm(){

    }

    @PostMapping("/external/vendor/form")
    public String pengajuaanVendor(@Valid Vendor vendor, RedirectAttributes attributes){
        vendor.setTanggalPengajuan(LocalDate.now());
        vendor.setStatus(StatusRecord.AKTIF);
        vendor.setStatusPengajuan("WAITING");
        vendor.setStatusPenilaian("BELUM_DINILAI");
        vendorDao.save(vendor);

        attributes.addFlashAttribute("success", "Berhasil");
        return "redirect:/external/vendor/form";
    }

    @PostMapping("/external/reg")
    public String registerGa(@Valid User user, RedirectAttributes attributes,
                             String jk, String nik, String noTlp){
        Karyawan karyawan = new Karyawan();
        User cek = userDao.findByUsername(user.getUsername());
        Role role = roleDao.findById("external").get();
        System.out.println("cek : " + role);
        if (cek == null){
            user.setActive(true);
            user.setRole(role);
            userDao.save(user);

            karyawan.setEmail(user.getUsername());
            karyawan.setUser(user);
            karyawan.setNamaKaryawan(user.getUser());
            karyawan.setNik(nik);
            karyawan.setTelpon(noTlp);
            karyawan.setJenisKelamin(jk);
            karyawan.setStatusAktif(StatusRecord.AKTIF);
            karyawan.setStatus(StatusRecord.AKTIF);
            karyawanDao.save(karyawan);

            Jabatan jabatan = jabatanDao.findById("6ed41126-e0e9-4dcf-8402-f0e8ba60e0d3").get();
            KaryawanJabatan karyawanJabatan = new KaryawanJabatan();
            karyawanJabatan.setKaryawan(karyawan);
            karyawanJabatan.setJabatan(jabatan);
            karyawanJabatan.setStatus(StatusRecord.AKTIF);
            karyawanJabatan.setStatusAktif("AKTIF");
            karyawanJabatanDao.save(karyawanJabatan);
            attributes.addFlashAttribute("cek", "Berhasil");
            return "redirect:/login";
        }else {
            attributes.addFlashAttribute("cek", "Email Anda sudah terdaftar");
            return "redirect:/external/form";
        }
    }

    @GetMapping("/external/peminjaman/list")
    public void peminjamanList(Model model, @RequestParam String bulan, @RequestParam String tahun){
        model.addAttribute("bulan", bulan);
        model.addAttribute("tahun", tahun);
        model.addAttribute("peminjaman", "active");
        if (bulan.equals("01")){
            model.addAttribute("namaBulan", "Januari");
        } else if (bulan.equals("02")){
            model.addAttribute("namaBulan", "Februari");
        } else if (bulan.equals("03")){
            model.addAttribute("namaBulan", "Maret");
        } else if (bulan.equals("04")){
            model.addAttribute("namaBulan", "April");
        } else if (bulan.equals("05")){
            model.addAttribute("namaBulan", "Mei");
        } else if (bulan.equals("06")){
            model.addAttribute("namaBulan", "Juni");
        } else if (bulan.equals("07")){
            model.addAttribute("namaBulan", "Juli");
        } else if (bulan.equals("08")){
            model.addAttribute("namaBulan", "Agustus");
        } else if (bulan.equals("09")){
            model.addAttribute("namaBulan", "September");
        } else if (bulan.equals("10")){
            model.addAttribute("namaBulan", "Oktober");
        } else if (bulan.equals("11")){
            model.addAttribute("namaBulan", "November");
        } else if (bulan.equals("12")){
            model.addAttribute("namaBulan", "Desember");
        }

        if (bulan.equals("01") || bulan.equals("03")|| bulan.equals("05") || bulan.equals("07") || bulan.equals("08") || bulan.equals("10") || bulan.equals("12")){
            model.addAttribute("tanggal", 31);
        } else if(bulan.equals("02")){
            model.addAttribute("tanggal", 28);
        }else if(bulan.equals("04") || bulan.equals("06") || bulan.equals("09") || bulan.equals("11")){
            model.addAttribute("tanggal", 30);
        }
        model.addAttribute("ruangan", ruanganDao.findByStatusAndStatusRuangan(StatusRecord.AKTIF, "BISA_DIPINJAMKAN"));


        model.addAttribute("peminjamanRuangan", peminjamanRuanganDao.findByStatusAndBulanAndTahun(StatusRecord.AKTIF, bulan, tahun));
    }

    @GetMapping("/external/pengadaan/list")
    public void pengadaanList(Model model, @PageableDefault(size = 15)Pageable pageable){
        model.addAttribute("pengadaanAset", pengadaanAsetDao.findByStatusOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, pageable));
        model.addAttribute("pengadaan", "active");
    }

    @GetMapping("/external/perbaikanSapras/list")
    public void perbaikanSaprasList(Model model, @PageableDefault(size = 20) Pageable pageable){
        model.addAttribute("list", perbaikanSaprasDao.findByStatusOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, pageable));
        model.addAttribute("perbaikan", "active");
    }

    @GetMapping("/external/peminjaman/kendaraan")
    public void peminjamanKendaraan(Model model, @RequestParam String bulan, @RequestParam String tahun){
        model.addAttribute("bulan", bulan);
        model.addAttribute("tahun", tahun);
        model.addAttribute("menuKendaraan", "active");
        if (bulan.equals("01")){
            model.addAttribute("namaBulan", "Januari");
        } else if (bulan.equals("02")){
            model.addAttribute("namaBulan", "Februari");
        } else if (bulan.equals("03")){
            model.addAttribute("namaBulan", "Maret");
        } else if (bulan.equals("04")){
            model.addAttribute("namaBulan", "April");
        } else if (bulan.equals("05")){
            model.addAttribute("namaBulan", "Mei");
        } else if (bulan.equals("06")){
            model.addAttribute("namaBulan", "Juni");
        } else if (bulan.equals("07")){
            model.addAttribute("namaBulan", "Juli");
        } else if (bulan.equals("08")){
            model.addAttribute("namaBulan", "Agustus");
        } else if (bulan.equals("09")){
            model.addAttribute("namaBulan", "September");
        } else if (bulan.equals("10")){
            model.addAttribute("namaBulan", "Oktober");
        } else if (bulan.equals("11")){
            model.addAttribute("namaBulan", "November");
        } else if (bulan.equals("12")){
            model.addAttribute("namaBulan", "Desember");
        }

        if (bulan.equals("01") || bulan.equals("03")|| bulan.equals("05") || bulan.equals("07") || bulan.equals("08") || bulan.equals("10") || bulan.equals("12")){
            model.addAttribute("tanggal", 31);
        } else if(bulan.equals("02")){
            model.addAttribute("tanggal", 28);
        }else if(bulan.equals("04") || bulan.equals("06") || bulan.equals("09") || bulan.equals("11")){
            model.addAttribute("tanggal", 30);
        }

        model.addAttribute("listPinjam", peminjamanKendaraanDao.findByStatusAndBulanAndTahun(StatusRecord.AKTIF, bulan, tahun));
    }

    @GetMapping("/external/aset/list")
    public void asetList(){

    }

    @GetMapping("/external/usermanual")
    public void userManual(HttpServletResponse response) throws Exception{
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=user_manual.pdf");
        FileCopyUtils.copy(userManual.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }


    @PostMapping("/external/perbaikanSapras")
    public String perbaikanSapras(@Valid PerbaikanSapras perbaikanSapras, @RequestParam("image") MultipartFile file) throws Exception{
        if (!file.isEmpty() || file != null){
            String namaFile = file.getName();
            String namaAsliFile = file.getOriginalFilename();
            String extension = "";

            int i = namaAsliFile.lastIndexOf('.');
            int p = Math.max(namaAsliFile.lastIndexOf('/'), namaAsliFile.lastIndexOf('\\'));
            if (i > p){
                extension = namaAsliFile.substring(i + 1);
            }

            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadBukti + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
            perbaikanSapras.setBukti(idFile + "." + extension);
        }else {
            perbaikanSapras.setBukti(perbaikanSapras.getBukti());
        }

        perbaikanSapras.setStatus(StatusRecord.AKTIF);
        perbaikanSapras.setStatusApproved("WAITING");
        perbaikanSapras.setTanggalPengajuan(LocalDate.now());
        perbaikanSaprasDao.save(perbaikanSapras);


        Mustache templateEmail = mustacheFactory.compile("templates/email/infoPerbaikan.html");
        Map<String, String> data = new HashMap<>();
        data.put("namaAset", perbaikanSapras.getAset().getNamaAset());
        data.put("lokasiBarang", perbaikanSapras.getRuangan().getNamaRuangan());
//        data.put("namaPengaju", perbaikanSapras.getNamaPengaju());
//        data.put("sebagai", perbaikanSapras.getSebagai());
        data.put("tanggalPengajuan", perbaikanSapras.getTanggalPengajuan().toString());
        data.put("keterangan", perbaikanSapras.getKeterangan());
        StringWriter output = new StringWriter();
        templateEmail.execute(output, data);
        
        gmailApiService.kirimEmail(
                "Permohonan Perbaikan Barang",
                "arifrahmatullah099@gmail.com",
                "Permohonan Perbaikan Barang oleh ",
                output.toString()
        );

        return "redirect:/external/perbaikanSapras/list";
    }


}
