package id.ac.tazkia.umum.aplikasiumum.dao.config;

import id.ac.tazkia.umum.aplikasiumum.entity.config.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleDao extends PagingAndSortingRepository<Role, String> {


}
