package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.KampusDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Kampus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Arrays;

@Controller
public class CampusController {
    @Autowired
    private KampusDao kampusDao;

    @GetMapping("/masters/campus")
    public String listKampus(Model model,@RequestParam(required = false)String search, @PageableDefault(size = 10)Pageable pageable){

        if (StringUtils.hasText(search)){
            model.addAttribute("listCampus", kampusDao.findByStatusNotInAndNamaKampusContainingIgnoreCaseOrderByNamaKampus(Arrays.asList((StatusRecord.HAPUS)), search, pageable));
        }else {
            model.addAttribute("listCampus", kampusDao.findByStatusOrderByNamaKampusDesc(StatusRecord.AKTIF, pageable));
        }
        return "masters/campus/list";
    }

    @GetMapping("/masters/campus/new")
    public String formCampus(Model model){
        model.addAttribute("campus", new Kampus());
        return "masters/campus/form";
    }

    @GetMapping("/masters/campus/edit")
    public String editCampus(Model model, @RequestParam(required = false) String id){
        model.addAttribute("campus", kampusDao.findById(id));
        return "masters/campus/form";
    }

    @PostMapping("/masters/campus/new")
    public String prosesNewCampus(@Valid Kampus kampus){
        if (kampus.getStatus() == null){
            kampus.setStatus(StatusRecord.AKTIF);
        }
        kampusDao.save(kampus);
        return "redirect:../campus";
    }

    @PostMapping("/masters/campus/delete")
    public String deleteCampus(@RequestParam Kampus kampus, RedirectAttributes attributes){
        kampus.setStatus(StatusRecord.HAPUS);
        kampusDao.save(kampus);
        attributes.addFlashAttribute("delete", "Delete Data Successfully");
        return "redirect:../campus";
    }
}
