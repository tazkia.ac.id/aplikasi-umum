package id.ac.tazkia.umum.aplikasiumum.dao.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.request.VendorKategoriPenilaian;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;


public interface VendorKategoriPenilaianDao extends PagingAndSortingRepository<VendorKategoriPenilaian, String> {
    List<VendorKategoriPenilaian> findByStatus(StatusRecord statusRecord);
}
