package id.ac.tazkia.umum.aplikasiumum.controller.request;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.*;
import id.ac.tazkia.umum.aplikasiumum.dao.request.*;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.*;
import id.ac.tazkia.umum.aplikasiumum.entity.request.*;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Controller @Slf4j
public class PeminjamanSaprasController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private AsetDao asetDao;

    @Value("${upload.bukti}")
    private String uploadBukti;

    @Value("${upload.lampiran}")
    private String uploadLampiran;

    @Autowired
    private PeminjamanAsetDao peminjamanAsetDao;

    @Autowired
    private RuanganDao ruanganDao;

    @Autowired
    private PeminjamanRuanganDao peminjamanRuanganDao;

    @Autowired
    private KaryawanDao karyawanDao;



    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private JabatanDao jabatanDao;

    @Autowired
    private SettingApprovalDao settingApprovalDao;

    @Autowired
    private PeminjamanKendaraanDao peminjamanKendaraanDao;

    @Autowired
    private PeminjamanKendaraanApprovalDao peminjamanKendaraanApprovalDao;

    @Autowired
    private PeminjamanRuanganApprovalDao peminjamanRuanganApprovalDao;

    @Autowired
    private AsetPengadaanApprovalDao asetPengadaanApprovalDao;



    @ModelAttribute("ruangan")
    public Iterable<Ruangan> listRoom(){
        return ruanganDao.findByStatusRuanganAndStatus("BISA_DIPINJAMKAN", StatusRecord.AKTIF);
    }

    @GetMapping("/request/peminjamanSapras/list")
    public void peminjamanSapras(Model model, @PageableDefault(size = 10) Pageable pageable,
                                 Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        model.addAttribute("kendaraan", peminjamanKendaraanDao.findByStatusAndKaryawanOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, karyawan, pageable));
        model.addAttribute("listApprovalRuangan", peminjamanRuanganApprovalDao.listApproval());
        model.addAttribute("listApprovalKendaraan", peminjamanKendaraanApprovalDao.listApproval());
    }

    @GetMapping("/request/peminjamanSapras/listRuangan")
    public void peminjamanSaprasRuangan(Model model, @PageableDefault Pageable pageable,
                                        Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        model.addAttribute("ruangan", peminjamanRuanganDao.findByStatusAndKaryawanOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, karyawan, pageable));
        model.addAttribute("listApprovalRuangan", peminjamanRuanganApprovalDao.listApproval());
    }



    @GetMapping("/request/peminjamanSapras/ruangan")
    public void peminjamanRuangan(Model model, @RequestParam String bulan, @RequestParam String tahun){
        model.addAttribute("bulan", bulan);
        model.addAttribute("tahun", tahun);
        if (bulan.equals("01")){
            model.addAttribute("namaBulan", "Januari");
        } else if (bulan.equals("02")){
            model.addAttribute("namaBulan", "Februari");
        } else if (bulan.equals("03")){
            model.addAttribute("namaBulan", "Maret");
        } else if (bulan.equals("04")){
            model.addAttribute("namaBulan", "April");
        } else if (bulan.equals("05")){
            model.addAttribute("namaBulan", "Mei");
        } else if (bulan.equals("06")){
            model.addAttribute("namaBulan", "Juni");
        } else if (bulan.equals("07")){
            model.addAttribute("namaBulan", "Juli");
        } else if (bulan.equals("08")){
            model.addAttribute("namaBulan", "Agustus");
        } else if (bulan.equals("09")){
            model.addAttribute("namaBulan", "September");
        } else if (bulan.equals("10")){
            model.addAttribute("namaBulan", "Oktober");
        } else if (bulan.equals("11")){
            model.addAttribute("namaBulan", "November");
        } else if (bulan.equals("12")){
            model.addAttribute("namaBulan", "Desember");
        }

        if (bulan.equals("01") || bulan.equals("03")|| bulan.equals("05") || bulan.equals("07") || bulan.equals("08") || bulan.equals("10") || bulan.equals("12")){
            model.addAttribute("tanggal", 31);
        } else if(bulan.equals("02")){
            model.addAttribute("tanggal", 28);
        }else if(bulan.equals("04") || bulan.equals("06") || bulan.equals("09") || bulan.equals("11")){
            model.addAttribute("tanggal", 30);
        }

        model.addAttribute("ruangan", ruanganDao.findByStatusAndStatusRuangan(StatusRecord.AKTIF, "BISA_DIPINJAMKAN"));
        model.addAttribute("peminjamanRuangan", peminjamanRuanganDao.findByStatusAndBulanAndTahun(StatusRecord.AKTIF, bulan, tahun));
    }

    @PostMapping("/request/peminjamanSapras/pinjamRuangan")
    public String requestPeminjamanRuangan(@Valid PeminjamanRuangan peminjamanRuangan,
                                           @RequestParam("image") MultipartFile file,
                                           Authentication authentication, RedirectAttributes attributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        KaryawanJabatan karyawanJabatan = karyawanJabatanDao.findByStatusAndKaryawan(StatusRecord.AKTIF, karyawan);
        Jabatan jabatan = jabatanDao.findById(karyawanJabatan.getJabatan().getId()).get();
        List<SettingApproval> settingApprovals = settingApprovalDao.findByStatusAndApproval(StatusRecord.AKTIF, jabatan.getId());
        Integer countTotalApproval = settingApprovalDao.countApproval(jabatan.getId());

        if (!file.isEmpty() || file != null){
            String namaAsli = file.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadLampiran + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);

            peminjamanRuangan.setLampiran(idFile + "." + extension);

            String bulan = peminjamanRuangan.getTanggalPeminjaman();
            String bl = bulan.substring(5, 7);
            System.out.println("bulan : " + bl);
            String tahun = peminjamanRuangan.getTanggalPeminjaman();
            String th = tahun.substring(0, 4);
            System.out.println("tahun : " + th);

            Ruangan ruangan = ruanganDao.findById(peminjamanRuangan.getRuangan().getId()).get();
            PeminjamanRuangan peminjamanRuangan1 = peminjamanRuanganDao.findByStatusAndTanggalPeminjamanAndRuangan(StatusRecord.AKTIF, peminjamanRuangan.getTanggalPeminjaman(), ruangan);
            if (peminjamanRuangan1 != null){
                attributes.addFlashAttribute("gagal", "Gagal");
                return "redirect:/request/peminjamanSapras/ruangan?bulan=" + bl + "&tahun=" + th;
            }



            String jamMulai = peminjamanRuangan.getWaktuPeminjamanMulaiString();

            String jam = "00";
            String menit = "00";
            String detik = "00";
            Integer jamInt = 00;
            String jam2 = "00";
            String menit2 = "00";
            String detik2 = "00";
            Integer jamInt2 = 00;

            if(jamMulai.length() == 4){
                jam = jamMulai.substring(0,1);
                menit = jamMulai.substring(2,4);
            } else if (jamMulai.length() == 5){
                jam = jamMulai.substring(0,2);
                menit = jamMulai.substring(3,5);
            } else if (jamMulai.length() == 7){
                jam = jamMulai.substring(0,1);
                menit = jamMulai.substring(2,4);
                jamInt = new Integer(jam);
                if (jamMulai.substring(5,7).equals("PM")){
                    jamInt = jamInt + 12;
                }
                jam = jamInt.toString();
                if (jam.length() == 1){
                    jam = '0'+jam;
                }
            }else{
                jam = jamMulai.substring(0,2);
                menit = jamMulai.substring(3,5);
                jamInt = new Integer(jam);
                if (jamMulai.substring(6,8).equals("PM")){
                    jamInt = jamInt + 12;
                }
                jam = jamInt.toString();
                if (jam.length() == 1){
                    jam = '0'+jam;
                }
            }


            String jamSelesai = peminjamanRuangan.getWaktuPeminjamanBerakhirString();

            if(jamSelesai.length() == 4){
                jam2 = jamSelesai.substring(0,1);
                menit2 = jamSelesai.substring(2,4);
            } else if (jamSelesai.length() == 5){
                jam2 = jamSelesai.substring(0,2);
                menit2 = jamSelesai.substring(3,5);
            } else if (jamSelesai.length() == 7){
                jam2 = jamSelesai.substring(0,1);
                menit2 = jamSelesai.substring(2,4);
                jamInt2 = new Integer(jam2);
                if (jamSelesai.substring(5,7).equals("PM")){
                    jamInt2 = jamInt2 + 12;
                }
                jam2 = jamInt2.toString();
                if (jam2.length() == 1){
                    jam2 = '0'+jam2;
                }
            }else{
                jam2 = jamSelesai.substring(0,2);
                menit2 = jamSelesai.substring(3,5);
                jamInt2 = new Integer(jam2);
                if (jamSelesai.substring(6,8).equals("PM")){
                    jamInt2 = jamInt2 + 12;
                }
                jam2 = jamInt2.toString();
                if (jam2.length() == 1){
                    jam2 = '0'+jam2;
                }
            }


            System.out.println("masuks :"+ jamMulai);
            System.out.println("keluars :"+ jamSelesai);


            jamMulai = jam + ':' + menit + ':' + detik;
            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
            LocalTime localTime = LocalTime.parse(jamMulai, formatter1);

            jamSelesai = jam2 + ':' + menit2 + ':' + detik2;
            DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("[HH][:mm][:ss]");
            LocalTime localTime2 = LocalTime.parse(jamSelesai, formatter2);

            LocalTime localTime1 = LocalTime.parse(jamMulai);
            LocalTime localTime3 = LocalTime.parse(jamSelesai);


            peminjamanRuangan.setKaryawan(karyawan);
            peminjamanRuangan.setWaktuPeminjamanMulai(localTime);
            peminjamanRuangan.setWaktuPeminjamanBerakhir(localTime2);
            peminjamanRuangan.setTanggalPengajuan(LocalDate.now());
            peminjamanRuangan.setStatus(StatusRecord.AKTIF);
            peminjamanRuangan.setStatusPeminjaman("WAITING");
            peminjamanRuangan.setBulan(bl);
            peminjamanRuangan.setTahun(th);
            peminjamanRuangan.setNomor(1);
            peminjamanRuangan.setTotalNomor(countTotalApproval);
            peminjamanRuanganDao.save(peminjamanRuangan);

            for (SettingApproval approval: settingApprovals){
                List<Jabatan> listJabatan = jabatanDao.findByStatusAndId(StatusRecord.AKTIF, approval.getIdJabatan().getId());
                for (Jabatan valueJabatan: listJabatan) {
                    System.out.println("test : " + listJabatan);
                    List<KaryawanJabatan> karyawanJabatanList = karyawanJabatanDao.findByStatusAndJabatan(StatusRecord.AKTIF, valueJabatan);
                    System.out.println("test t : " + karyawanJabatanList);
                    for (KaryawanJabatan valueKaryawanJabatan : karyawanJabatanList) {
                        PeminjamanRuanganApproval peminjamanRuanganApproval = new PeminjamanRuanganApproval();
                        peminjamanRuanganApproval.setPeminjamanRuangan(peminjamanRuangan);
                        peminjamanRuanganApproval.setSettingApproval(approval);
                        peminjamanRuanganApproval.setNomor(approval.getUrutan());
                        peminjamanRuanganApproval.setIdKaryawan(valueKaryawanJabatan.getKaryawan().getId());
                        peminjamanRuanganApproval.setStatus(StatusRecord.WAITING);
                        peminjamanRuanganApprovalDao.save(peminjamanRuanganApproval);
                    }
                }
            }

        } else {
            attributes.addFlashAttribute("gagal", "gagal");
        }


        return "redirect:/request/peminjamanSapras/listRuangan";
    }

    @PostMapping("/request/peminjamanRuangan/cancel")
    public String cancelPeminjamanRuangan(@RequestParam PeminjamanRuangan peminjamanRuangan){
        peminjamanRuangan.setStatus(StatusRecord.HAPUS);
        peminjamanRuanganDao.save(peminjamanRuangan);
        return "redirect:/request/peminjamanSapras/listRuangan";
    }

    @PostMapping("/request/peminjamanRuangan/uploadLampiran")
    public String uploadLampiran(@Valid PeminjamanRuangan peminjamanRuangan, @RequestParam("image") MultipartFile file, RedirectAttributes attributes) throws Exception{
        PeminjamanRuangan data = peminjamanRuanganDao.findById(peminjamanRuangan.getId()).get();
        if (!file.isEmpty() || file != null) {
            String namaAsli = file.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadLampiran + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);

            data.setLampiran(idFile + "." + extension);
            peminjamanRuanganDao.save(data);
        }

        return "redirect:/request/peminjamanSapras/listRuangan";
    }


//    List Approval Kendaraan



    @GetMapping("/masters/pdf/{lampiran}/")
    public ResponseEntity<byte[]> detailMobil(@PathVariable String lampiran) throws Exception{
        PeminjamanRuangan peminjamanRuangan = peminjamanRuanganDao.findById(lampiran).get();
        String lokasiFile = uploadLampiran + File.separator + peminjamanRuangan.getLampiran();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (peminjamanRuangan.getLampiran().toLowerCase().endsWith("jpeg") || peminjamanRuangan.getLampiran().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(peminjamanRuangan.getLampiran().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(peminjamanRuangan.getLampiran().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
