package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import id.ac.tazkia.umum.aplikasiumum.dao.config.UserDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.KaryawanDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.MobilDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanKendaraanDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Mobil;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanKendaraan;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
public class JadwalKendaraanController {

    @Autowired
    private PeminjamanKendaraanDao peminjamanKendaraanDao;

    @Autowired
    private MobilDao mobilDao;

    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private UserDao userDao;
    @Autowired
    private KaryawanDao karyawanDao;


    @ModelAttribute("kendaraan")
    private Iterable<Mobil> listKendaraan(){
        return mobilDao.findByStatus(StatusRecord.AKTIF);
    }

    @GetMapping("/masters/jadwalKendaraan")
    public String getJadwalKendaraan(Model model, @PageableDefault(size = 20)Pageable pageable){
        model.addAttribute("jadwalKendaraan", "active");
        model.addAttribute("listData", peminjamanKendaraanDao.findByStatusOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, pageable));
        return "masters/jadwalKendaraan/list";
    }

    @PostMapping("/masters/jadwalKendaraan/post")
    public String postJadwalKendaraan(@Valid PeminjamanKendaraan peminjamanKendaraan, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        peminjamanKendaraan.setKaryawan(karyawan);
        peminjamanKendaraan.setStatusPeminjaman("APPROVED");
        peminjamanKendaraan.setStatus(StatusRecord.AKTIF);
        peminjamanKendaraan.setTanggalPengajuan(LocalDate.now());
        peminjamanKendaraanDao.save(peminjamanKendaraan);
        return "redirect:/masters/jadwalKendaraan";
    }
}
