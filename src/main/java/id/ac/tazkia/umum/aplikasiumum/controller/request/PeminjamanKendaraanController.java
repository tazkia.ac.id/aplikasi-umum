package id.ac.tazkia.umum.aplikasiumum.controller.request;

import id.ac.tazkia.umum.aplikasiumum.dao.config.UserDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.*;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanAsetDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanKendaraanApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanKendaraanDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.*;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanKendaraan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanKendaraanApproval;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Controller
public class PeminjamanKendaraanController {

    @Autowired
    private CurrentUserService currentUserService;

    @Value("${upload.bukti}")
    private String uploadBukti;

    @Value("${upload.lampiran}")
    private String uploadLampiran;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private JabatanDao jabatanDao;

    @Autowired
    private SettingApprovalDao settingApprovalDao;

    @Autowired
    private MobilDao mobilDao;

    @ModelAttribute("kendaraan")
    private Iterable<Mobil> listKendaraan(){
        return mobilDao.findByStatus(StatusRecord.AKTIF);
    }

    @Autowired
    private PeminjamanKendaraanDao peminjamanKendaraanDao;

    @Autowired
    private PeminjamanKendaraanApprovalDao peminjamanKendaraanApprovalDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private PeminjamanAsetDao peminjamanAsetDao;

    @GetMapping("/request/peminjamanSapras/listKendaraan")
    public void peminjamanSaprasKendaraan(Model model, @PageableDefault Pageable pageable,
                                          Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        model.addAttribute("kendaraan", peminjamanKendaraanDao.findByStatusAndKaryawanOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, karyawan, pageable));
        model.addAttribute("listApprovalKendaraan", peminjamanKendaraanApprovalDao.listApproval());
    }

    @GetMapping("/request/peminjamanSapras/kendaraan")
    public void peminjamanKendaraan(Model model, @RequestParam(required = false) String tujuan, @RequestParam(required = false) String keperluan,
                                    @RequestParam(required = false) String tanggalPinjam){


        model.addAttribute("tujuan", tujuan);
        model.addAttribute("keperluan", keperluan);
        model.addAttribute("tanggalPinjam", tanggalPinjam);

        Object peminjamanKendaraanList = peminjamanKendaraanDao.cekPeminjamanKendaraan(tanggalPinjam);
        if (peminjamanKendaraanList == null){
            model.addAttribute("cek", "kosong");
        }else {
            model.addAttribute("cek", "ada");
            model.addAttribute("text", peminjamanKendaraanList);
        }

    }

    @PostMapping("/request/peminjamanSapras/kendaraan")
    public String requestPeminjamanKendaraan(@Valid PeminjamanKendaraan peminjamanKendaraan, @RequestParam("image") MultipartFile file,
                                             Authentication authentication, RedirectAttributes attributes) throws Exception {
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        KaryawanJabatan karyawanJabatan = karyawanJabatanDao.findByStatusAndKaryawan(StatusRecord.AKTIF, karyawan);
        Jabatan jabatan = jabatanDao.findById(karyawanJabatan.getJabatan().getId()).get();
        List<SettingApproval> settingApproval = settingApprovalDao.findByStatusAndApproval(StatusRecord.AKTIF, jabatan.getId());
        Integer countTotalApproval = settingApprovalDao.countApproval(jabatan.getId());

        if (!file.isEmpty() || file != null){
            String namaAsli = file.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadLampiran + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);

            peminjamanKendaraan.setLampiran(idFile + "." + extension);

            Mobil mobil = mobilDao.findById(peminjamanKendaraan.getIdMobil().getId()).get();
            PeminjamanKendaraan peminjamanKendaraan1 = peminjamanKendaraanDao.findByStatusAndTanggalPinjamAndIdMobil(StatusRecord.AKTIF, peminjamanKendaraan.getTanggalPinjam(), mobil);
            if (peminjamanKendaraan1 != null){
                attributes.addFlashAttribute("gagal", "Gagal");
                return "redirect:/request/peminjamanSapras/kendaraan?bulan=" + peminjamanKendaraan.getBulan() + "&tahun=" + peminjamanKendaraan.getTahun();
            }

            LocalDate bulan = peminjamanKendaraan.getTanggalPinjam();
            String bln = bulan.toString();
            LocalDate tahun = peminjamanKendaraan.getTanggalPinjam();
            String thn = tahun.toString();



            peminjamanKendaraan.setKaryawan(karyawan);
            peminjamanKendaraan.setTanggalPengajuan(LocalDate.now());
            peminjamanKendaraan.setStatus(StatusRecord.AKTIF);
            peminjamanKendaraan.setStatusPeminjaman("WAITING");
            peminjamanKendaraan.setStatusPembayaran("BELUM_DIBAYAR");
            peminjamanKendaraan.setBulan(bln.substring(5, 6));
            peminjamanKendaraan.setTahun(thn.substring(0, 3));
            peminjamanKendaraan.setNomor(1);
            peminjamanKendaraan.setTotalNomor(countTotalApproval);
            peminjamanKendaraanDao.save(peminjamanKendaraan);

            for (SettingApproval approval: settingApproval){

                List<Jabatan> listJabatan = jabatanDao.findByStatusAndId(StatusRecord.AKTIF, approval.getIdJabatan().getId());
                for (Jabatan valueJabatan: listJabatan){
                    System.out.println("test : " + listJabatan);
                    List<KaryawanJabatan> karyawanJabatanList = karyawanJabatanDao.findByStatusAndJabatan(StatusRecord.AKTIF, valueJabatan);
                    System.out.println("test t : " + karyawanJabatanList);
                    for (KaryawanJabatan valueKaryawanJabatan : karyawanJabatanList){
                        PeminjamanKendaraanApproval peminjamanKendaraanApproval = new PeminjamanKendaraanApproval();
                        peminjamanKendaraanApproval.setPeminjamanKendaraan(peminjamanKendaraan);
                        peminjamanKendaraanApproval.setIdKaryawan(valueKaryawanJabatan.getKaryawan().getId());
                        peminjamanKendaraanApproval.setSettingApproval(approval);
                        peminjamanKendaraanApproval.setStatus(StatusRecord.WAITING);
                        peminjamanKendaraanApproval.setNomor(approval.getUrutan());
                        peminjamanKendaraanApprovalDao.save(peminjamanKendaraanApproval);
                    }
                }

            }

            attributes.addFlashAttribute("berhasil", "Berhasil");
        }else {
            attributes.addFlashAttribute("gagal", "Berhasil");
        }




        return "redirect:/request/peminjamanSapras/listKendaraan";
    }

    @GetMapping("/peminjamanKendaraan/pembayaran")
    public String peminjamanKendaraanPembayaran(Model model, @RequestParam(required = false) String id){
        PeminjamanKendaraan peminjamanKendaraan = peminjamanKendaraanDao.findById(id).get();
        model.addAttribute("id", peminjamanKendaraan.getId());
        model.addAttribute("kendaraan", peminjamanKendaraan.getIdMobil().getMerek());
        model.addAttribute("karyawan", peminjamanKendaraan.getKaryawan().getNamaKaryawan());
        model.addAttribute("tarif", peminjamanKendaraan.getIdMobil().getTarif());
        return "request/peminjamanSapras/pembayaran";
    }

    @PostMapping("/request/peminjamanSapras/pembayaran")
    public String pembayaranPeminjamanKendaraan(@RequestParam PeminjamanKendaraan peminjamanKendaraan, MultipartFile bukti, RedirectAttributes attributes) throws Exception{
        if (!bukti.isEmpty() || bukti != null){
            String namaFile = bukti.getName();
            String namaAsliFile = bukti.getOriginalFilename();
            String extension = "";

            int i = namaAsliFile.lastIndexOf('.');
            int p = Math.max(namaAsliFile.lastIndexOf('/'), namaAsliFile.lastIndexOf('\\'));
            if (i > p){
                extension = namaAsliFile.substring(i + 1);
            }

            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadBukti + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            bukti.transferTo(tujuan);
            peminjamanKendaraan.setBukti(idFile + "." + extension);
        }else {
            peminjamanKendaraan.setBukti(peminjamanKendaraan.getBukti());
        }
        peminjamanKendaraan.setStatusPembayaran("SUDAH_DIBAYAR");

        peminjamanKendaraanDao.save(peminjamanKendaraan);
        attributes.addFlashAttribute("pembayaran", "Berhasil");
        return "redirect:/request/peminjamanSapras/list";
    }

    @PostMapping("/request/peminjamanSapras/kendaraan/delete")
    public String delete(@RequestParam PeminjamanKendaraan peminjamanKendaraan){
        peminjamanKendaraan.setStatus(StatusRecord.HAPUS);
        peminjamanKendaraanDao.save(peminjamanKendaraan);
        return "redirect:/request/peminjamanSapras/listKendaraan";
    }

    @GetMapping("/cekPeminjam")
    @ResponseBody
    public String cekPeminjam(@RequestParam(required = false) String tanggalPinjam){
        LocalDate date = LocalDate.parse(tanggalPinjam);
        List<PeminjamanKendaraan> baseResponses = peminjamanKendaraanDao.findByStatusAndTanggalPinjam(StatusRecord.AKTIF, date);
        return baseResponses.toString();
    }

}
