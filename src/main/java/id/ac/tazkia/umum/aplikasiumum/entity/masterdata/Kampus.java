package id.ac.tazkia.umum.aplikasiumum.entity.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.GenericGenerators;

import javax.persistence.*;

@Data
@Entity
public class Kampus {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String kodeKampus;
    private String namaKampus;
    private String keterangan;
    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}
