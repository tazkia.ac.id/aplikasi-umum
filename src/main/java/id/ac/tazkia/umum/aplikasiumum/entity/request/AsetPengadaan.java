package id.ac.tazkia.umum.aplikasiumum.entity.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.AsetJenis;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class AsetPengadaan {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    private String namaAset;

    private String spekAset;

    private String jumlah;

    private String satuan;

    private Integer hargaAset;

    private Integer totalHarga;

    private String kegunaan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String statusApproved;

    private LocalDate tanggalPengajuan;
    private LocalDate tanggalApproved;

    private String prosesPengadaan;

    private String statusPembayaran;

    private String buktiPembayaran;

    private String statusProses;

    private String keteranganDitolak;

    private Integer nomor;
    private Integer totalNomor;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tglPenyerahan;

    private String buktiPenyerahan;

    private String lampiran;
}
