package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.DepartemenDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.JabatanDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Departemen;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Gedung;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Jabatan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Arrays;

@Controller
public class JobPositionController {

    @Autowired
    private JabatanDao jabatanDao;

    @Autowired
    private DepartemenDao departemenDao;

    @ModelAttribute("departemen")
    public Iterable<Departemen> listDepartemen() {
        return departemenDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS));
    }

    @GetMapping("/masters/jobPosition")
    public String jobPositionList(Model model, @RequestParam(required = false)String search,
                                  @PageableDefault(size = 10) Pageable page){

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listJobPosition", jabatanDao.findByStatusAndKodeJabatanContainingIgnoreCaseOrNamaJabatanContainingIgnoreCaseOrderByKodeJabatan(StatusRecord.AKTIF, search,search, page));
        }else{
            model.addAttribute("listJobPosition", jabatanDao.findByStatusOrderByKodeJabatan(StatusRecord.AKTIF, page));
        }


        return "masters/position/list";

    }

    @GetMapping("/masters/jobPosition/new")
    public String formJobPosition(Model model){
        model.addAttribute("position",new Jabatan());

        return "masters/position/form";
    }

    @GetMapping("/masters/jobPosition/edit")
    public String editJobPosition(Model model, @RequestParam(required = false)String id){
        model.addAttribute("position", jabatanDao.findById(id));
        return "masters/position/form";
    }

    @PostMapping("/masters/jobPosition/new")
    public String saveJobPosition(@Valid Jabatan jabatan){
        jabatan.setStatusAktif("A");
        jabatan.setStatus(StatusRecord.AKTIF);

        jabatanDao.save(jabatan);
        return "redirect:../jobPosition";
    }

    @PostMapping("/masters/jobPosition/delete")
    public String deleteJobPosition(@RequestParam Jabatan jabatan){
        jabatan.setStatusAktif("H");
        jabatan.setStatus(StatusRecord.HAPUS);
        jabatanDao.save(jabatan);
        return "redirect:../jobPosition";
    }
}
