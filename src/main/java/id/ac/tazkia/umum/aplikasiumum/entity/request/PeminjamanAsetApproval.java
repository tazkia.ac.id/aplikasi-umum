package id.ac.tazkia.umum.aplikasiumum.entity.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.SettingApproval;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class PeminjamanAsetApproval {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_peminjaman_aset")
    private PeminjamanAset peminjamanAset;

    private String idKaryawan;

    @ManyToOne
    @JoinColumn(name = "id_setting_approval")
    private SettingApproval settingApproval;

    private Integer nomor;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalApproval;
}
