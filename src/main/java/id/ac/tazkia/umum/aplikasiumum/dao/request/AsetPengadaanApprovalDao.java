package id.ac.tazkia.umum.aplikasiumum.dao.request;

import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.AsetPengadaanApproval;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AsetPengadaanApprovalDao extends PagingAndSortingRepository<AsetPengadaanApproval, String> {
    @Query(value = "select b.id, f.nama_karyawan, d.nama_jabatan, c.urutan, a.status, a.tanggal_approve from aset_pengadaan_approval as a \n" +
            "\tinner join aset_pengadaan as b on b.id = a.id_aset_pengadaan\n" +
            "    inner join setting_approval as c on c.id = a.id_setting_approval\n" +
            "    inner join jabatan as d on d.id = c.id_jabatan\n" +
            "    inner join karyawan_jabatan as e on d.id = e.id_jabatan\n" +
            "    inner join karyawan as f on f.id = e.id_karyawan \n" +
            "\twhere e.status = 'AKTIF'\n" +
            "    order by c.urutan asc", nativeQuery = true)
    Object[] listApproved();

    @Query(value = "select b.id, a.id as idApproval, d.nama_karyawan as pengaju, nama_aset, spek_aset, jumlah, satuan, tanggal_pengajuan, a.status, b.lampiran from aset_pengadaan_approval as a\n" +
            "inner join aset_pengadaan as b on a.id_aset_pengadaan = b.id and a.nomor = b.nomor\n" +
            "inner join s_user as c on b.id_user = c.id\n" +
            "inner join karyawan as d on c.id = d.id_user\n" +
            "where a.status = 'WAITING' and b.status_approved = 'WAITING' and b.status = 'AKTIF'\n" +
            "and a.id_karyawan = ?1 order by b.tanggal_pengajuan desc", nativeQuery = true)
    Object[] listApproval(Karyawan karyawan);

    @Query(value = "select coalesce(count(b.id), 0) from aset_pengadaan_approval as a\n" +
            "inner join aset_pengadaan as b on a.id_aset_pengadaan = b.id and a.nomor = b.nomor\n" +
            "inner join s_user as c on b.id_user = c.id\n" +
            "inner join karyawan as d on c.id = d.id_user\n" +
            "where a.status = 'WAITING' and b.status_approved = 'WAITING' and b.status = 'AKTIF'\n" +
            "and a.id_karyawan = ?1", nativeQuery = true)
    Object[] countApprovalPengadaan(Karyawan karyawan);
}
