package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Mobil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MobilDao extends PagingAndSortingRepository<Mobil, String> {
    Page<Mobil> findByStatusNotIn(List<StatusRecord> statusRecords, Pageable pageable);
    Iterable<Mobil> findByStatus(StatusRecord statusRecord);
}
