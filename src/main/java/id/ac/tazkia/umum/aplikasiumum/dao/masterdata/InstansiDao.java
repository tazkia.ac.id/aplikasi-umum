package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;


import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Instansi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface InstansiDao extends PagingAndSortingRepository<Instansi, String> {

    List<Instansi> findByStatusOrderByNama(StatusRecord statusRecord);

    Page<Instansi> findByStatusOrderByNama(StatusRecord statusRecord, Pageable page);

    Page<Instansi> findByStatusAndNamaContainingOrderByNama(StatusRecord statusRecord, String nama, Pageable page);

}
