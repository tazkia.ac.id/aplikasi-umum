package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;


import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.AsetDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.RuanganDao;
import id.ac.tazkia.umum.aplikasiumum.dto.BaseRequestRuangan;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Controller
public class Repot {

    @Autowired
    private AsetDao asetDao;

    @Autowired
    private RuanganDao ruanganDao;

    @GetMapping("/report/download")
    public void excelDataAset(HttpServletResponse response)throws IOException {
        String[] column = {"No", "Nama Aset", "Status Aset", "Ruangan", "Gedung", "Jumlah" , "Harga Aset", "Kondisi Aset"};

        List<Object[]> listData = asetDao.downloadExcel();

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data Aset");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for(int i = 0; i < column.length; i++){
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(column[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1;
        int baris = 1;

        for (Object[] data: listData){
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(data[1].toString());
            if (data[2] == null){
                row.createCell(2).setCellValue("-");
            }else {
                row.createCell(2).setCellValue(data[2].toString());
            }
            row.createCell(3).setCellValue(data[3].toString());
            row.createCell(4).setCellValue(data[4].toString());
            row.createCell(5).setCellValue(data[5].toString());
            if (data[6] == null){
                row.createCell(6).setCellValue("-");
            }else {
                row.createCell(6).setCellValue(data[6].toString());
            }
            row.createCell(7).setCellValue((String) data[7]);
        }

        for (int i = 0; i < column.length; i++){
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=dataAset_"+ LocalDate.now()+".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();



    }

    @GetMapping("/report/perkategori")
    public void excelDataAsetPerKategori(HttpServletResponse response) throws IOException{
        List<BaseRequestRuangan> listData = ruanganDao.getRuangan();
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Data Aset");

        int rowCountTanah = 1;
        int baris = 1;
        Row rowTanah = sheet.createRow(rowCountTanah);
        rowTanah.createCell(0).setCellValue("Tanah");
        Row rowTanahHeader = sheet.createRow(rowCountTanah + 1);
        rowTanahHeader.createCell(0).setCellValue("No.");
        rowTanahHeader.createCell(1).setCellValue("Nama");
        rowTanahHeader.createCell(2).setCellValue("Merek/Type");
        rowTanahHeader.createCell(3).setCellValue("Tanggal Perolehan");
        rowTanahHeader.createCell(4).setCellValue("Harga");

        List<Object[]> tanah = asetDao.donwloadAsetPerkategori("Tanah");

        int rowDataTanahNum1 = 3;
        for (Object[] data: tanah){
            Row row = sheet.createRow(rowDataTanahNum1++);
            row.createCell(0).setCellValue(baris++);

            row.createCell(1).setCellValue(data[0].toString());
            if (data[1] == null){
                row.createCell(2).setCellValue("-");
            }else {
                row.createCell(2).setCellValue(data[1].toString());
            }

            if (data[2] == null){
                row.createCell(3).setCellValue("-");
            }else {
                row.createCell(3).setCellValue(data[2].toString());
            }

            if (data[3] == null){
                row.createCell(4).setCellValue("-");
            }else {
                row.createCell(4).setCellValue(data[3].toString());
            }

        }

        Row rowPeralatanFurnitureKantor = sheet.createRow(rowDataTanahNum1 + 1);
        rowPeralatanFurnitureKantor.createCell(0).setCellValue("Bangunan");
        Row rowPeralatanFurnitureKantorHeader = sheet.createRow(rowDataTanahNum1 + 2);
        rowPeralatanFurnitureKantorHeader.createCell(0).setCellValue("No.");
        rowPeralatanFurnitureKantorHeader.createCell(1).setCellValue("Nama");
        rowPeralatanFurnitureKantorHeader.createCell(2).setCellValue("Merek/Type");
        rowPeralatanFurnitureKantorHeader.createCell(3).setCellValue("Tanggal Perolehan");
        rowPeralatanFurnitureKantorHeader.createCell(4).setCellValue("Harga");

        List<Object[]> peralatanBangunan = asetDao.donwloadAsetPerkategori("Bangunan");

        int rowDataBangunan = rowDataTanahNum1 + 3;
        int baris1 = 1;
        for (Object[] data: peralatanBangunan){
            Row row = sheet.createRow(rowDataBangunan++);
            row.createCell(0).setCellValue(baris1++);

            row.createCell(1).setCellValue(data[0].toString());
            if (data[1] == null){
                row.createCell(2).setCellValue("-");
            }else {
                row.createCell(2).setCellValue(data[1].toString());
            }

            if (data[2] == null){
                row.createCell(3).setCellValue("-");
            }else {
                row.createCell(3).setCellValue(data[2].toString());
            }

            if (data[3] == null){
                row.createCell(4).setCellValue("-");
            }else {
                row.createCell(4).setCellValue(data[3].toString());
            }

        }

        int rowCountRenovasiBangunan = rowDataBangunan + 1;
        Row rowRenovasiBangunan = sheet.createRow(rowCountRenovasiBangunan);
        rowRenovasiBangunan.createCell(0).setCellValue("Renovasi Bangunan");

        Row rowRenovasiBangunanHeader = sheet.createRow(rowCountRenovasiBangunan + 1);
        rowRenovasiBangunanHeader.createCell(0).setCellValue("No.");
        rowRenovasiBangunanHeader.createCell(1).setCellValue("Nama");
        rowRenovasiBangunanHeader.createCell(2).setCellValue("Merek/Type");
        rowRenovasiBangunanHeader.createCell(3).setCellValue("Tanggal Perolehan");
        rowRenovasiBangunanHeader.createCell(4).setCellValue("Harga");

        List<Object[]> renovasiBangunan = asetDao.donwloadAsetPerkategori("RenovasiBangunan");

        int rowDataRenovasiBangunan = rowCountRenovasiBangunan + 2;
        int baris2 = 1;
        for (Object[] data: renovasiBangunan){
            Row row = sheet.createRow(rowDataRenovasiBangunan++);
            row.createCell(0).setCellValue(baris2++);

            row.createCell(1).setCellValue(data[0].toString());
            if (data[1] == null){
                row.createCell(2).setCellValue("-");
            }else {
                row.createCell(2).setCellValue(data[1].toString());
            }

            if (data[2] == null){
                row.createCell(3).setCellValue("-");
            }else {
                row.createCell(3).setCellValue(data[2].toString());
            }

            if (data[3] == null){
                row.createCell(4).setCellValue("-");
            }else {
                row.createCell(4).setCellValue(data[3].toString());
            }

        }

        int rowCountPeralatanKelas = rowDataRenovasiBangunan;
        Row rowPeralatanKelas = sheet.createRow(rowCountPeralatanKelas + 1);
        rowPeralatanKelas.createCell(0).setCellValue("Peralatan Kelas");
        Row rowPeralatanKelasHeader = sheet.createRow(rowCountPeralatanKelas + 2);
        rowPeralatanKelasHeader.createCell(0).setCellValue("No.");
        rowPeralatanKelasHeader.createCell(1).setCellValue("Nama");
        rowPeralatanKelasHeader.createCell(2).setCellValue("Merek/Type");
        rowPeralatanKelasHeader.createCell(3).setCellValue("Tanggal Perolehan");
        rowPeralatanKelasHeader.createCell(4).setCellValue("Harga");

        List<Object[]> peralatanKelas = asetDao.donwloadAsetPerkategori("PeralatanKelas");
        int rowDataPeralatanKelas = rowCountPeralatanKelas + 3;
        int baris3 = 1;
        for (Object[] data: peralatanKelas){
            Row row = sheet.createRow(rowDataPeralatanKelas++);
            row.createCell(0).setCellValue(baris3++);

            row.createCell(1).setCellValue(data[0].toString());
            if (data[1] == null){
                row.createCell(2).setCellValue("-");
            }else {
                row.createCell(2).setCellValue(data[1].toString());
            }

            if (data[2] == null){
                row.createCell(3).setCellValue("-");
            }else {
                row.createCell(3).setCellValue(data[2].toString());
            }

            if (data[3] == null){
                row.createCell(4).setCellValue("-");
            }else {
                row.createCell(4).setCellValue(data[3].toString());
            }

        }

        int rowCountPeralatanKantor = rowDataPeralatanKelas;
        Row rowPeralatanKantor = sheet.createRow(rowCountPeralatanKantor + 1);
        rowPeralatanKantor.createCell(0).setCellValue("Peralatan Kantor");
        Row rowPeralatanKantorHeader = sheet.createRow(rowCountPeralatanKantor + 2);
        rowPeralatanKantorHeader.createCell(0).setCellValue("No.");
        rowPeralatanKantorHeader.createCell(1).setCellValue("Nama");
        rowPeralatanKantorHeader.createCell(2).setCellValue("Merek/Type");
        rowPeralatanKantorHeader.createCell(3).setCellValue("Tanggal Perolehan");
        rowPeralatanKantorHeader.createCell(4).setCellValue("Harga");

        List<Object[]> peralatanKantor = asetDao.donwloadAsetPerkategori("PeralatanKantor");
        int rowDataPeralatanKantor = rowCountPeralatanKantor + 3;
        int baris4 = 1;
        for (Object[] data: peralatanKantor){
            Row row = sheet.createRow(rowDataPeralatanKantor++);
            row.createCell(0).setCellValue(baris4++);

            row.createCell(1).setCellValue(data[0].toString());
            if (data[1] == null){
                row.createCell(2).setCellValue("-");
            }else {
                row.createCell(2).setCellValue(data[1].toString());
            }

            if (data[2] == null){
                row.createCell(3).setCellValue("-");
            }else {
                row.createCell(3).setCellValue(data[2].toString());
            }

            if (data[3] == null){
                row.createCell(4).setCellValue("-");
            }else {
                row.createCell(4).setCellValue(data[3].toString());
            }

        }

        int rowCountPeralatanAsrama = rowDataPeralatanKantor;
        Row rowPeralatanAsrama = sheet.createRow(rowCountPeralatanAsrama + 1);
        rowPeralatanAsrama.createCell(0).setCellValue("Peralatan Asrama");
        Row rowPeralatanAsramaHeader = sheet.createRow(rowCountPeralatanAsrama + 2);
        rowPeralatanAsramaHeader.createCell(0).setCellValue("No.");
        rowPeralatanAsramaHeader.createCell(1).setCellValue("Nama");
        rowPeralatanAsramaHeader.createCell(2).setCellValue("Merek/Type");
        rowPeralatanAsramaHeader.createCell(3).setCellValue("Tanggal Perolehan");
        rowPeralatanAsramaHeader.createCell(4).setCellValue("Harga");

        List<Object[]> peralatanAsrama = asetDao.donwloadAsetPerkategori("PeralatanAsrama");
        int rowDataPeralatanAsrama = rowCountPeralatanAsrama + 3;
        int baris5 = 1;
        for (Object[] data: peralatanAsrama){
            Row row = sheet.createRow(rowDataPeralatanAsrama++);
            row.createCell(0).setCellValue(baris5++);

            row.createCell(1).setCellValue(data[0].toString());
            if (data[1] == null){
                row.createCell(2).setCellValue("-");
            }else {
                row.createCell(2).setCellValue(data[1].toString());
            }

            if (data[2] == null){
                row.createCell(3).setCellValue("-");
            }else {
                row.createCell(3).setCellValue(data[2].toString());
            }

            if (data[3] == null){
                row.createCell(4).setCellValue("-");
            }else {
                row.createCell(4).setCellValue(data[3].toString());
            }

        }

        int rowCountFurnitureKelas = rowDataPeralatanAsrama;
        Row rowFurnitureKelas = sheet.createRow(rowCountFurnitureKelas + 1);
        rowFurnitureKelas.createCell(0).setCellValue("Peralatan Furniture Kelas");
        Row rowFurnitureKelasHeader = sheet.createRow(rowCountFurnitureKelas + 2);
        rowFurnitureKelasHeader.createCell(0).setCellValue("No.");
        rowFurnitureKelasHeader.createCell(1).setCellValue("Nama");
        rowFurnitureKelasHeader.createCell(2).setCellValue("Merek/Type");
        rowFurnitureKelasHeader.createCell(3).setCellValue("Tanggal Perolehan");
        rowFurnitureKelasHeader.createCell(4).setCellValue("Harga");

        List<Object[]> peralatanFurnitureKelas = asetDao.donwloadAsetPerkategori("PeralatanFurnitureKelas");
        int rowDataFurnitureKelas = rowCountFurnitureKelas + 3;
        int baris6 = 1;
        for (Object[] data: peralatanFurnitureKelas){
            Row row = sheet.createRow(rowDataFurnitureKelas++);
            row.createCell(0).setCellValue(baris6++);

            row.createCell(1).setCellValue(data[0].toString());
            if (data[1] == null){
                row.createCell(2).setCellValue("-");
            }else {
                row.createCell(2).setCellValue(data[1].toString());
            }

            if (data[2] == null){
                row.createCell(3).setCellValue("-");
            }else {
                row.createCell(3).setCellValue(data[2].toString());
            }

            if (data[3] == null){
                row.createCell(4).setCellValue("-");
            }else {
                row.createCell(4).setCellValue(data[3].toString());
            }

        }

        int rowCountFurnitureKantor = rowDataFurnitureKelas;
        Row rowFurnitureKantor = sheet.createRow(rowCountFurnitureKantor + 1);
        rowFurnitureKantor.createCell(0).setCellValue("Peralatan Furniture Kantor");
        Row rowFurnitureKantorHeader = sheet.createRow(rowCountFurnitureKantor + 2);
        rowFurnitureKantorHeader.createCell(0).setCellValue("No.");
        rowFurnitureKantorHeader.createCell(1).setCellValue("Nama");
        rowFurnitureKantorHeader.createCell(2).setCellValue("Merek/Type");
        rowFurnitureKantorHeader.createCell(3).setCellValue("Tanggal Perolehan");
        rowFurnitureKantorHeader.createCell(4).setCellValue("Harga");

        List<Object[]> peralatanFurnitureKantor = asetDao.donwloadAsetPerkategori("PeralatanFurnitureKantor");
        int rowDataFurnitureKantor = rowDataFurnitureKelas + 3;
        int baris7 = 1;
        for (Object[] data: peralatanFurnitureKantor){
            Row row = sheet.createRow(rowDataFurnitureKantor++);
            row.createCell(0).setCellValue(baris7++);

            row.createCell(1).setCellValue(data[0].toString());
            if (data[1] == null){
                row.createCell(2).setCellValue("-");
            }else {
                row.createCell(2).setCellValue(data[1].toString());
            }

            if (data[2] == null){
                row.createCell(3).setCellValue("-");
            }else {
                row.createCell(3).setCellValue(data[2].toString());
            }

            if (data[3] == null){
                row.createCell(4).setCellValue("-");
            }else {
                row.createCell(4).setCellValue(data[3].toString());
            }

        }

        int rowCountKendaraan = rowDataFurnitureKantor;
        Row rowKendaraan = sheet.createRow(rowCountKendaraan + 1);
        rowKendaraan.createCell(0).setCellValue("Kendaraan Leasing");
        Row rowKendaraanHeader = sheet.createRow(rowCountKendaraan + 2);
        rowKendaraanHeader.createCell(0).setCellValue("No.");
        rowKendaraanHeader.createCell(1).setCellValue("Nama");
        rowKendaraanHeader.createCell(2).setCellValue("Merek/Type");
        rowKendaraanHeader.createCell(3).setCellValue("Tanggal Perolehan");
        rowKendaraanHeader.createCell(4).setCellValue("Harga");


        List<Object[]> kendaraan = asetDao.donwloadAsetPerkategori("KendaraanLeasing");
        int rowDataKendaraan = rowDataFurnitureKantor + 3;
        int baris8 = 1;
        for (Object[] data: kendaraan){
            Row row = sheet.createRow(rowDataKendaraan++);
            row.createCell(0).setCellValue(baris8++);

            row.createCell(1).setCellValue(data[0].toString());
            if (data[1] == null){
                row.createCell(2).setCellValue("-");
            }else {
                row.createCell(2).setCellValue(data[1].toString());
            }

            if (data[2] == null){
                row.createCell(3).setCellValue("-");
            }else {
                row.createCell(3).setCellValue(data[2].toString());
            }

            if (data[3] == null){
                row.createCell(4).setCellValue("-");
            }else {
                row.createCell(4).setCellValue(data[3].toString());
            }

        }





        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=dataAset_"+ LocalDate.now()+".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }
}
