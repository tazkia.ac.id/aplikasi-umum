package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.GedungDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.KampusDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Gedung;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Arrays;

@Controller
public class BuildingController {
    @Autowired
    private GedungDao gedungDao;
    @Autowired
    private KampusDao kampusDao;

    @GetMapping("/masters/building")
    public String listBuilding(Model model, @RequestParam(required = false)String search, @PageableDefault(size = 10)Pageable pageable){
        if (StringUtils.hasText(search)){
            model.addAttribute("listBuilding", gedungDao.findByStatusNotInAndNamaGedungContainingIgnoreCaseOrderByNamaGedung(Arrays.asList((StatusRecord.HAPUS)), search, pageable));
        }else {
            model.addAttribute("listBuilding", gedungDao.findByStatus(StatusRecord.AKTIF, pageable));
        }

        return "masters/building/list";
    }

    @GetMapping("/masters/building/new")
    public String formBuilding(Model model){
        model.addAttribute("building", new Gedung());
        model.addAttribute("kampus", kampusDao.findByStatus(StatusRecord.AKTIF));
        return "masters/building/form";
    }

    @GetMapping("/masters/building/edit")
    public String editBuilding(Model model, @RequestParam(required = false)String id){
        model.addAttribute("building", gedungDao.findById(id));
        model.addAttribute("kampus", kampusDao.findByStatus(StatusRecord.AKTIF));
        return "masters/building/form";
    }

    @PostMapping("/masters/building/new")
    public String newBuilding(@Valid Gedung gedung){
        gedung.setStatus(StatusRecord.AKTIF);
        gedungDao.save(gedung);
        return "redirect:../building";
    }

    @PostMapping("/masters/building/delete")
    public String deleteBuilding(@RequestParam Gedung gedung, RedirectAttributes attributes){
        gedung.setStatus(StatusRecord.HAPUS);
        gedungDao.save(gedung);
        attributes.addFlashAttribute("delete", "Delete Data Successfully");
        return "redirect:../building";
    }
}
