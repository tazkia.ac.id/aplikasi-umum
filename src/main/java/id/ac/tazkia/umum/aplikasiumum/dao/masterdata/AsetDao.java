package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Aset;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.AsetJenis;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Ruangan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AsetDao extends PagingAndSortingRepository<Aset, String> {
    Page<Aset> findByStatusNotInOrderByNamaAsetAsc(List<StatusRecord> status, Pageable pageable);
    Page<Aset> findByStatusNotInAndNamaAsetContainingIgnoreCaseOrderByNamaAsetAsc(List<StatusRecord> status, String search, Pageable pageable);


    @Query(value = "select a.id, a.nama_aset, b.barcode, c.jenis_aset, d.nama_ruangan, a.jumlah, a.kondisi_aset, a.keterangan from aset as a \n" +
            "left join aset_barcode as b on a.id = b.id_aset \n" +
            "inner join aset_jenis as c on c.id = a.id_jenis_aset\n" +
            "inner join ruangan as d on d.id = a.id_ruangan\n" +
            "order by a.nama_aset ASC", nativeQuery = true)
    List<Object[]> listAset();

    List<Aset> findByBarcode(String barcode);

    List<Aset> findByStatusAndAsetJenisAndStatusPeminjaman(StatusRecord statusRecord, AsetJenis asetJenis, String statusPeminjaman);
    Page<Aset> findByStatusNotInAndRuanganOrderByNamaAset(List<StatusRecord> status, Ruangan ruangan, Pageable pageable);
    Page<Aset> findByStatusAndRuanganOrderByNamaAset(StatusRecord statusRecord, Ruangan ruangan, Pageable pageable);
    Page<Aset> findByStatusNotInAndAsetJenisOrderByAsetJenis(List<StatusRecord> status, AsetJenis asetJenis, Pageable pageable);
    Long countAsetByStatus(StatusRecord statusRecord);

    List<Aset> findByStatusAndRuangan(StatusRecord statusRecord, Ruangan ruangan);
    List<Aset> findByStatusAndStatusPeminjaman(StatusRecord statusRecord, String statusPeminjaman);
    List<Aset> findByStatus(StatusRecord statusRecord);


    @Query(value = "select a.id, a.nama_aset, a.status_asset, c.nama_ruangan, d.nama_gedung, a.jumlah, a.harga_aset ,a.kondisi_aset from aset as a \n" +
            "\tinner join aset_jenis as b on a.id_jenis_aset = b.id\n" +
            "    inner join ruangan as c on c.id = a.id_ruangan\n" +
            "    inner join gedung as d on d.id = c.id_gedung \n" +
            "\twhere a.status = 'AKTIF'\n" +
            "    order by c.nama_ruangan", nativeQuery = true)
    List<Object[]> downloadExcel();

    @Query(value = "SELECT nama_aset, merek, tanggal_beli, harga_aset FROM aset where kategori = ?1", nativeQuery = true)
    List<Object[]> donwloadAsetPerkategori(String id);

    @Query(value = "SELECT * FROM aset where barcode is null", nativeQuery = true)
    List<Aset> listBarcodeNull();
}
