package id.ac.tazkia.umum.aplikasiumum.dao.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Mobil;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanKendaraan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;

public interface PeminjamanKendaraanDao extends PagingAndSortingRepository<PeminjamanKendaraan, String> {
    List<PeminjamanKendaraan> findByStatusAndBulanAndTahun(StatusRecord statusRecord, String bulan, String tahun);
    Page<PeminjamanKendaraan> findByStatusAndKaryawanOrderByTanggalPengajuanDesc(StatusRecord statusRecord, Karyawan karyawan, Pageable pageable);
    PeminjamanKendaraan findByStatusAndTanggalPinjamAndIdMobil(StatusRecord statusRecord, LocalDate tanggal, Mobil mobil);

    List<PeminjamanKendaraan> findByStatusAndTanggalPinjam(StatusRecord statusRecord, LocalDate date);

    @Query(value = "select id, id_mobil from peminjaman_kendaraan where status = 'AKTIF' and tanggal_pinjam = ?1", nativeQuery = true)
    Object cekPeminjamanKendaraan(String tanggalPinjam);

    Page<PeminjamanKendaraan> findByStatusOrderByTanggalPengajuanDesc(StatusRecord statusRecord, Pageable pageable);
    Page<PeminjamanKendaraan> findByStatusAndStatusPeminjamanAndStatusPembayaranOrderByTanggalPengajuanDesc(StatusRecord statusRecord, String statusPeminjaman, String statusPembayaran,Pageable pageable);
}
