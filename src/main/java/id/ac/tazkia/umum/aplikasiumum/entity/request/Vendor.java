package id.ac.tazkia.umum.aplikasiumum.entity.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.VendorJenis;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Data
@Entity
public class Vendor {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_karyawan")
    private Karyawan karyawan;

    private String namaPt;
    private String alamatPt;

    @ManyToOne
    @JoinColumn(name = "jenis_penawaran")
    private VendorJenis jenisPenawaran;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
    private String statusPenilaian;
    private LocalDate tanggalPengajuan;
    private String statusPengajuan;
    private String keteranganRejected;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalPresentasi;

    private String keterangan;

    private String lampiran;

}
