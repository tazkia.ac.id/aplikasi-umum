package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.VendorJenis;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface VendorJenisDao extends PagingAndSortingRepository<VendorJenis, String> {
    Page<VendorJenis> findByStatus(StatusRecord statusRecord, Pageable pageable);
    Iterable<VendorJenis> findByStatus(StatusRecord statusRecord);
}
