package id.ac.tazkia.umum.aplikasiumum.dao.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.VendorJenis;
import id.ac.tazkia.umum.aplikasiumum.entity.request.Vendor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface VendorDao extends PagingAndSortingRepository<Vendor, String> {
    Page<Vendor> findByStatusOrderByTanggalPengajuanDesc(StatusRecord statusRecord, Pageable pageable);
    Page<Vendor> findByStatusAndJenisPenawaran(StatusRecord statusRecord, VendorJenis vendorJenis, Pageable pageable);

    List<Vendor> findByStatusAndKaryawan(StatusRecord statusRecord, Karyawan karyawan);
}
