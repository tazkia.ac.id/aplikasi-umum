package id.ac.tazkia.umum.aplikasiumum.entity.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Ruangan;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Entity
public class PeminjamanRuangan {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_ruangan")
    private Ruangan ruangan;

    @ManyToOne
    @JoinColumn(name = "id_karyawan")
    private Karyawan karyawan;

    private String tanggalPeminjaman;
    private String tanggalPeminjamanBerakhir;

    private LocalTime waktuPeminjamanMulai;
    private String waktuPeminjamanMulaiString;

    private LocalTime waktuPeminjamanBerakhir;
    private String waktuPeminjamanBerakhirString;

    private String kegiatan;

    private String statusPeminjaman;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private LocalDate tanggalPengajuan;
    private String bulan;
    private String tahun;

    private Integer nomor;
    private Integer totalNomor;

    private String lampiran;

}
