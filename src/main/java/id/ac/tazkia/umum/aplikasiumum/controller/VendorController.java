package id.ac.tazkia.umum.aplikasiumum.controller;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.KaryawanDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.VendorJenisDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.VendorDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.VendorKategoriPenilaianDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.VendorNilaiDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.VendorJenis;
import id.ac.tazkia.umum.aplikasiumum.entity.request.AsetPengadaan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.Vendor;
import id.ac.tazkia.umum.aplikasiumum.entity.request.VendorKategoriPenilaian;
import id.ac.tazkia.umum.aplikasiumum.entity.request.VendorNilai;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.UUID;

@Controller @Slf4j
public class VendorController {
    @Autowired
    private VendorDao vendorDao;

    @Value("${upload.lampiran}")
    private String uploadLampiran;

    @Autowired
    private VendorKategoriPenilaianDao vendorKategoriPenilaianDao;

    @Autowired
    private VendorNilaiDao vendorNilaiDao;

    @Autowired
    private VendorJenisDao vendorJenisDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @ModelAttribute("vendorJenis")
    public Iterable<VendorJenis> listVendorJenis(){
        return vendorJenisDao.findByStatus(StatusRecord.AKTIF);
    }

    @GetMapping("/vendor/list")
    public void getListVendor(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        model.addAttribute("list", vendorDao.findByStatusAndKaryawan(StatusRecord.AKTIF, karyawan));
    }

    @GetMapping("/vendor/form")
    public void getFormVendor(){



    }

    @GetMapping("/vendor/image/{lampiran}/")
    public ResponseEntity<byte[]> detailMobil(@PathVariable String lampiran) throws Exception{
        Vendor vendor = vendorDao.findById(lampiran).get();
        String lokasiFile = uploadLampiran + File.separator + vendor.getLampiran();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (vendor.getLampiran().toLowerCase().endsWith("jpeg") || vendor.getLampiran().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(vendor.getLampiran().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(vendor.getLampiran().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/vendor/form")
    public String saveVendor(@Valid Vendor vendor, @RequestParam("image") MultipartFile file, Authentication authentication) throws Exception{
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        if (!file.isEmpty() || file != null){
            String namaFile = file.getName();
            String namaAsliFile = file.getOriginalFilename();
            String extension = "";

            int i = namaAsliFile.lastIndexOf('.');
            int p = Math.max(namaAsliFile.lastIndexOf('/'), namaAsliFile.lastIndexOf('\\'));
            if (i > p){
                extension = namaAsliFile.substring(i + 1);
            }

            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadLampiran + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
            vendor.setLampiran(idFile + "." + extension);
        }else {
            vendor.setLampiran(vendor.getLampiran());
        }


        vendor.setKaryawan(karyawan);
        vendor.setTanggalPengajuan(LocalDate.now());
        vendor.setStatus(StatusRecord.AKTIF);
        vendor.setStatusPengajuan("WAITING");
        vendor.setStatusPenilaian("BELUM_DINILAI");
        vendorDao.save(vendor);

        return "redirect:/vendor/list";
    }

    @GetMapping("/approval/vendor/list")
    public void listVendor(Model model, @PageableDefault Pageable pageable, @RequestParam(required = false) VendorJenis vendorJenis){
        if (vendorJenis != null){
            model.addAttribute("list", vendorDao.findByStatusAndJenisPenawaran(StatusRecord.AKTIF, vendorJenis, pageable));
            model.addAttribute("nilai", vendorNilaiDao.listNilai());
            model.addAttribute("kategoriPenilaian", vendorKategoriPenilaianDao.findByStatus(StatusRecord.AKTIF));
        }else {
            model.addAttribute("kategoriPenilaian", vendorKategoriPenilaianDao.findByStatus(StatusRecord.AKTIF));
            model.addAttribute("list", vendorDao.findByStatusOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, pageable));
            model.addAttribute("nilai", vendorNilaiDao.listNilai());
        }

    }

    @PostMapping("/approval/vendor/approved")
    public String approvedVendor(@RequestParam Vendor vendor){
        vendor.setStatusPengajuan("APPROVED");
        vendorDao.save(vendor);
        return "redirect:/approval/vendor/list";
    }

    @PostMapping("/vendor/batal")
    public String batalVendor(@RequestParam Vendor vendor){
        vendor.setStatus(StatusRecord.HAPUS);
        vendorDao.save(vendor);
        return "redirect:/vendor/list";
    }

    @PostMapping("/approval/vendor/rejected")
    public String rejectedVendor(@Valid Vendor vendor){
        Vendor vn = vendorDao.findById(vendor.getId()).get();
        vendor.setNamaPt(vn.getNamaPt());
        vendor.setAlamatPt(vn.getAlamatPt());
        vendor.setJenisPenawaran(vn.getJenisPenawaran());
        vendor.setStatus(vn.getStatus());
        vendor.setStatusPengajuan("REJECTED");
        vendor.setTanggalPengajuan(vn.getTanggalPengajuan());
        vendorDao.save(vendor);
        return "redirect:/approval/vendor/list";
    }

    @PostMapping("/approval/vendor/aturjadwal")
    public String aturJadwalVendor(@Valid Vendor vendor, RedirectAttributes attributes){
        Vendor vn = vendorDao.findById(vendor.getId()).get();
        vendor.setNamaPt(vn.getNamaPt());
        vendor.setAlamatPt(vn.getAlamatPt());
        vendor.setJenisPenawaran(vn.getJenisPenawaran());
        vendor.setStatus(vn.getStatus());
        vendor.setStatusPengajuan(vn.getStatusPengajuan());
        vendor.setTanggalPengajuan(vn.getTanggalPengajuan());
        vendorDao.save(vendor);
        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:/approval/vendor/list";
    }

    @PostMapping("/approval/vendor/rate")
    public String rateVendor(@RequestParam String[] idKategoriPenilaian, HttpServletRequest request, RedirectAttributes attributes){
        String idVendor = request.getParameter("idVendor");
        Vendor vendor = vendorDao.findById(idVendor).get();
        vendor.setStatusPenilaian("SUDAH_DINILAI");
        vendorDao.save(vendor);
        for (String penilaian : idKategoriPenilaian){
            VendorKategoriPenilaian vendorKategoriPenilaian = vendorKategoriPenilaianDao.findById(penilaian).get();
            String nilai = request.getParameter(vendorKategoriPenilaian.getId() + "+nilai");
            Float score = Float.parseFloat(nilai);

            VendorNilai vendorNilai = new VendorNilai();
            vendorNilai.setVendor(vendor);
            vendorNilai.setPenilaian(vendorKategoriPenilaian);
            vendorNilai.setNilai(score);
            vendorNilai.setStatus(StatusRecord.AKTIF);
            vendorNilaiDao.save(vendorNilai);
            System.out.println("test : " + nilai);
        }
        attributes.addFlashAttribute("oke", "Berhasil");
        return "redirect:/approval/vendor/list";
    }

}
