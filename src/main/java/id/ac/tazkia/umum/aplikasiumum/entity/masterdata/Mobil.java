package id.ac.tazkia.umum.aplikasiumum.entity.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
public class Mobil {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_departemen")
    private Departemen idDapartemen;

    private String jenis;
    private String merek;
    private String keterangan;
    private String warna;
    private String nomorPolisi;
    private String bulanPajak;
    private String tahun;
    private String file;
    private String supir;
    private String statusAktif;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String tarif;
    private String tarifLuar;


}
