package id.ac.tazkia.umum.aplikasiumum.controller.approval;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.KaryawanDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.AsetPengadaanApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanKendaraanApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanRuanganApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanRuanganDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanRuangan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanRuanganApproval;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;

@Controller
public class ApprovalPeminjamanRuangan {

    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private PeminjamanKendaraanApprovalDao peminjamanKendaraanApprovalDao;
    @Autowired
    private PeminjamanRuanganApprovalDao peminjamanRuanganApprovalDao;

    @Autowired
    private AsetPengadaanApprovalDao asetPengadaanApprovalDao;

    @Autowired
    private PeminjamanRuanganDao peminjamanRuanganDao;



    @GetMapping("/approval/peminjamanSapras/ruangan")
    public void approvalPeminjamanSaprasRuangan(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        model.addAttribute("listApproval", peminjamanRuanganApprovalDao.listAllApprovalRuangan(karyawan.getId()));
        model.addAttribute("jumlahApproval", peminjamanRuanganApprovalDao.countRuanganApproval(karyawan.getId()));
        model.addAttribute("jumlahApprovalKendaraan", peminjamanKendaraanApprovalDao.countApprovalKendaraan(karyawan.getId()));
        model.addAttribute("jumlahApprovalPengadaan", asetPengadaanApprovalDao.countApprovalPengadaan(karyawan));
    }

    @GetMapping("/approval/peminjamanSapras/listRuangan")
    public void approvedListRuangan(Model model, @PageableDefault Pageable pageable){
        model.addAttribute("list", peminjamanRuanganDao.findByStatusAndStatusPeminjamanOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, "APPROVED", pageable));
    }

    @PostMapping("/approval/peminjamanSapras/approved")
    public String approvedPeminjamanRuangan(@RequestParam PeminjamanRuangan peminjamanRuangan, @RequestParam PeminjamanRuanganApproval peminjamanRuanganApproval, RedirectAttributes attributes){
        Integer jumlahApproved = peminjamanRuangan.getNomor() + 1;

        if (jumlahApproved > peminjamanRuangan.getTotalNomor()){
            peminjamanRuangan.setStatusPeminjaman("APPROVED");
            peminjamanRuanganDao.save(peminjamanRuangan);
        }else {
            peminjamanRuangan.setNomor(jumlahApproved);
            peminjamanRuanganDao.save(peminjamanRuangan);
        }

        peminjamanRuanganApproval.setStatus(StatusRecord.APPROVED);
        peminjamanRuanganApproval.setTanggalApproval(LocalDate.now());
        peminjamanRuanganApprovalDao.save(peminjamanRuanganApproval);

        attributes.addFlashAttribute("approved", "berhasil");
        return "redirect:/approval/peminjamanSapras/ruangan?status=all";
    }

    @PostMapping("/approval/peminjamanSapras/rejected")
    public String rejectedPeminjamanRuangan(@RequestParam PeminjamanRuangan peminjamanRuangan, RedirectAttributes attributes){
        peminjamanRuangan.setStatusPeminjaman("REJECTED");
        peminjamanRuanganDao.save(peminjamanRuangan);
        attributes.addFlashAttribute("rejected", "gagal");
        return "redirect:/approval/peminjamanSapras/ruangan?status=all";
    }
}
