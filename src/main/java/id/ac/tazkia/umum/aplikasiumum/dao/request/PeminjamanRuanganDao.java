package id.ac.tazkia.umum.aplikasiumum.dao.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Ruangan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanRuangan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;
import java.util.List;

public interface PeminjamanRuanganDao extends PagingAndSortingRepository<PeminjamanRuangan, Serializable> {
    List<PeminjamanRuangan> findByStatus(StatusRecord statusRecord);
    PeminjamanRuangan findByStatusAndTanggalPeminjamanAndRuangan(StatusRecord statusRecord, String tanggal, Ruangan ruangan);
    List<PeminjamanRuangan> findByStatusAndBulanAndTahun(StatusRecord statusRecord, String bulan, String tahun);
    Page<PeminjamanRuangan> findByStatusAndKaryawanOrderByTanggalPengajuanDesc(StatusRecord statusRecord, Karyawan karyawan, Pageable pageable);

    Page<PeminjamanRuangan> findByStatusOrderByTanggalPengajuanDesc(StatusRecord statusRecord, Pageable pageable);
    Page<PeminjamanRuangan> findByStatusAndStatusPeminjamanOrderByTanggalPengajuanDesc(StatusRecord statusRecord, String statusPeminjaman, Pageable pageable);
}
