package id.ac.tazkia.umum.aplikasiumum.dao.request;

import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanKendaraanApproval;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PeminjamanKendaraanApprovalDao extends PagingAndSortingRepository<PeminjamanKendaraanApproval, String> {
    @Query(value = "select b.id, c.nama_karyawan, e.nama_jabatan, a.status, a.tanggal_approval from peminjaman_kendaraan_approval as a\n" +
            "            inner join peminjaman_kendaraan as b on a.id_peminjaman_kendaraan = b.id\n" +
            "            inner join karyawan as c on a.id_karyawan = c.id\n" +
            "            inner join karyawan_jabatan as d on d.id_karyawan = c.id\n" +
            "            inner join jabatan as e on d.id_jabatan = e.id \n" +
            "            where d.status = 'AKTIF' and c.status = 'AKTIF' and b.status = 'AKTIF'\n" +
            "            order by a.nomor asc", nativeQuery = true)
    Object[] listApproval();

    @Query(value = "select b.id, a.id as idApproval, c.nama_karyawan, d.merek, b.tanggal_pinjam,b.keperluan  from peminjaman_kendaraan_approval as a\n" +
            "\tinner join peminjaman_kendaraan as b on a.id_peminjaman_kendaraan = b.id and a.nomor = b.nomor\n" +
            "    inner join karyawan as c on b.id_karyawan = c.id\n" +
            "    inner join mobil as d on b.id_mobil = d.id\n" +
            "    where a.status = 'WAITING' and b.status_peminjaman = 'WAITING' and b.status = 'AKTIF'\n" +
            "    and a.id_karyawan = ?1", nativeQuery = true)
    Object[] listApprovalAllKendaraan(String karyawan);

    @Query(value = "select coalesce(count(b.id), 0) from peminjaman_kendaraan_approval as a\n" +
            "\tinner join peminjaman_kendaraan as b on a.id_peminjaman_kendaraan = b.id and a.nomor = b.nomor\n" +
            "    inner join karyawan as c on b.id_karyawan = c.id\n" +
            "    inner join mobil as d on b.id_mobil = d.id\n" +
            "    where a.status = 'WAITING' and b.status_peminjaman = 'WAITING' and b.status = 'AKTIF'\n" +
            "    and a.id_karyawan = ?1", nativeQuery = true)
    Integer countApprovalKendaraan(String karyawan);
}
