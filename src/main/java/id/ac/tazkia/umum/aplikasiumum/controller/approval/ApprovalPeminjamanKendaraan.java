package id.ac.tazkia.umum.aplikasiumum.controller.approval;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.*;
import id.ac.tazkia.umum.aplikasiumum.dao.request.AsetPengadaanApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanKendaraanApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanKendaraanDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanRuanganApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Mobil;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanKendaraan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanKendaraanApproval;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
    import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;

@Controller
public class ApprovalPeminjamanKendaraan {

    @Autowired
    private CurrentUserService currentUserService;

    @Value("${upload.bukti}")
    private String uploadBukti;

    @Value("${upload.lampiran}")
    private String uploadLampiran;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private JabatanDao jabatanDao;

    @Autowired
    private SettingApprovalDao settingApprovalDao;

    @Autowired
    private MobilDao mobilDao;

    @ModelAttribute("kendaraan")
    private Iterable<Mobil> listKendaraan(){
        return mobilDao.findByStatus(StatusRecord.AKTIF);
    }

    @Autowired
    private PeminjamanKendaraanDao peminjamanKendaraanDao;

    @Autowired
    private PeminjamanKendaraanApprovalDao peminjamanKendaraanApprovalDao;

    @Autowired
    private PeminjamanRuanganApprovalDao peminjamanRuanganApprovalDao;

    @Autowired
    private AsetPengadaanApprovalDao asetPengadaanApprovalDao;

    @GetMapping("/approval/peminjamanSapras/kendaraan")
    public void approvalPeminjamanSaprasKendaraan(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        model.addAttribute("listApproval", peminjamanKendaraanApprovalDao.listApprovalAllKendaraan(karyawan.getId()));
        model.addAttribute("jumlahApproval", peminjamanKendaraanApprovalDao.countApprovalKendaraan(karyawan.getId()));
        model.addAttribute("jumlahApprovalRuangan", peminjamanRuanganApprovalDao.countRuanganApproval(karyawan.getId()));
        model.addAttribute("jumlahApprovalPengadaan", asetPengadaanApprovalDao.countApprovalPengadaan(karyawan));
    }

    @PostMapping("/approval/peminjamanSapras/kendaraan/approved")
    public String approvedPeminjamanKendaraan(@RequestParam PeminjamanKendaraan peminjamanKendaraan, @RequestParam PeminjamanKendaraanApproval peminjamanKendaraanApproval, RedirectAttributes attributes){
        Integer jumlahApproved = peminjamanKendaraan.getNomor() + 1;

        if (jumlahApproved > peminjamanKendaraan.getTotalNomor()){
            peminjamanKendaraan.setStatusPeminjaman("APPROVED");
            peminjamanKendaraanDao.save(peminjamanKendaraan);
        }else {
            peminjamanKendaraan.setNomor(jumlahApproved);
            peminjamanKendaraanDao.save(peminjamanKendaraan);
        }

        peminjamanKendaraanApproval.setTanggalApproval(LocalDate.now());
        peminjamanKendaraanApproval.setStatus(StatusRecord.APPROVED);
        peminjamanKendaraanApprovalDao.save(peminjamanKendaraanApproval);


        return "redirect:/approval/peminjamanSapras/kendaraan";
    }

    @GetMapping("/approval/peminjamanSapras/listKendaraan")
    public void listApprovedKendaraan(Model model, @PageableDefault Pageable pageable, @RequestParam(required = false) String status){
        model.addAttribute("status", status);
        if (StringUtils.hasText(status)){
            model.addAttribute("list", peminjamanKendaraanDao.findByStatusAndStatusPeminjamanAndStatusPembayaranOrderByTanggalPengajuanDesc
                    (StatusRecord.AKTIF, "APPROVED", status,pageable));
        }
    }

    @PostMapping("/approval/peminjamanSapras/kendaraan/rejected")
    public String rejectedPeminjamanKendaraan(@RequestParam PeminjamanKendaraan peminjamanKendaraan, RedirectAttributes attributes){
        peminjamanKendaraan.setStatusPeminjaman("REJECTED");
        peminjamanKendaraanDao.save(peminjamanKendaraan);
        attributes.addFlashAttribute("rejected", "gagal");
        return "redirect:/approval/peminjamanSapras/kendaraan?status=all";
    }
}
