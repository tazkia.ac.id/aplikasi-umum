package id.ac.tazkia.umum.aplikasiumum.dto;

public interface BaseRequestRuangan {
    String getId();
    String getRuangan();
}
