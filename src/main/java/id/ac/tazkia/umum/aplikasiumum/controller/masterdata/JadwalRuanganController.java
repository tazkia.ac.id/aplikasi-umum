package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;


import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.KaryawanDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.RuanganDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanRuanganDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Ruangan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanRuangan;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.LocalDate;


@Controller
public class JadwalRuanganController {

    @Autowired
    CurrentUserService currentUserService;
    @Autowired
    private RuanganDao ruanganDao;

    @Autowired
    private PeminjamanRuanganDao peminjamanRuanganDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @ModelAttribute("ruangan")
    public Iterable<Ruangan> listRoom(){
        return ruanganDao.findByStatusRuanganAndStatus("BISA_DIPINJAMKAN", StatusRecord.AKTIF);
    }

    @GetMapping("/masters/jadwalRuangan")
    public String getJadwalRuangan(Model model, @PageableDefault(size = 20)Pageable pageable){
        model.addAttribute("jadwalRuangan", "active");
        model.addAttribute("listData", peminjamanRuanganDao.findByStatusOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, pageable));
        return "masters/jadwalRuangan/list";
    }

    @PostMapping("/masters/jadwalRuangan/post")
    public String postJadwalRuangan(@Valid PeminjamanRuangan peminjamanRuangan, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        peminjamanRuangan.setKaryawan(karyawan);
        peminjamanRuangan.setStatus(StatusRecord.AKTIF);
        peminjamanRuangan.setStatusPeminjaman("APPROVED");
        peminjamanRuangan.setTanggalPengajuan(LocalDate.now());
        peminjamanRuanganDao.save(peminjamanRuangan);


        return "redirect:/masters/jadwalRuangan";
    }
}
