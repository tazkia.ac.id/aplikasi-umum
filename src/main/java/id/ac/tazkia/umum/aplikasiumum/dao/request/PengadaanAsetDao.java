package id.ac.tazkia.umum.aplikasiumum.dao.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.request.AsetPengadaan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PengadaanAsetDao extends PagingAndSortingRepository<AsetPengadaan, String> {
    Page<AsetPengadaan> findByStatusAndUserOrderByTanggalPengajuanDesc(StatusRecord status, User user, Pageable pageable);
    Page<AsetPengadaan> findByStatusOrderByTanggalPengajuanDesc(StatusRecord statusRecord, Pageable pageable);
    Page<AsetPengadaan> findByStatusAndStatusApprovedOrderByTanggalApprovedDesc(StatusRecord statusRecord, String statusApproved, Pageable pageable);
    Page<AsetPengadaan> findByStatusAndStatusApprovedAndStatusPembayaranOrderByTanggalApprovedDesc(StatusRecord statusRecord, String statusApproved, String status, Pageable pageable);
    Page<AsetPengadaan> findByStatusAndStatusPembayaranOrderByTanggalApprovedDesc(StatusRecord statusRecord, String statusPembayaran, Pageable pageable);
}
