package id.ac.tazkia.umum.aplikasiumum.entity.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class Gedung {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uudi", strategy = "uudi2")
    private String id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_kampus")
    private Kampus idKampus;

    private String kodeGedung;

    private String namaGedung;

    private String keterangan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
}
