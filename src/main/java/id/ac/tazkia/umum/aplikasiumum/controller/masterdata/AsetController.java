package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.AsetDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.AsetJenisDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.AsetPerawatanDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.RuanganDao;
import id.ac.tazkia.umum.aplikasiumum.dto.UpdateHargaAset;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusAset;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.awt.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.List;

@Controller @Slf4j
public class AsetController {

    @Autowired
    private AsetDao asetDao;
    @Autowired
    private AsetJenisDao asetJenisDao;

    @Autowired
    private RuanganDao ruanganDao;

    @Autowired
    private AsetPerawatanDao asetPerawatanDao;

    @Value("${upload.qrcode}")
    private String uploadQrcode;

    @Value("${upload.asset}")
    private String uploadAsset;


    @ModelAttribute("asetCategory")
    public Iterable<AsetJenis> listAsetCategory(){
        return asetJenisDao.findByStatusOrderByJenisAsetAsc(StatusRecord.AKTIF);
    }

    @ModelAttribute("roomList")
    public Iterable<Ruangan> listRoom(){
        return ruanganDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS));
    }

    @GetMapping("/masters/categoryAsset")
    public String categoryAssetList(Model model, @PageableDefault(size = 10)Pageable pageable){
        model.addAttribute("listCategoryAset", asetJenisDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS), pageable));
        return "masters/categoryAsset/list";
    }

    @PostMapping("/categoryAsset/save")
    public String categoryAssetSave(@Valid AsetJenis asetJenis, RedirectAttributes attributes){
        asetJenis.setStatus(StatusRecord.AKTIF);
        asetJenisDao.save(asetJenis);
        attributes.addFlashAttribute("success", "Data saved successfully");
        return "redirect:/masters/categoryAsset";
    }

    @PostMapping("/categoryAsset/delete")
    public String categoryAssetDelete(@RequestParam AsetJenis asetJenis, RedirectAttributes attributes){
        asetJenis.setStatus(StatusRecord.HAPUS);
        asetJenisDao.save(asetJenis);
        attributes.addFlashAttribute("delete", "Data deleted successfully");
        return "redirect:/masters/categoryAsset";
    }

    @GetMapping("/masters/asset")
    public String assetList(Model model, @RequestParam(required = false)String search,
                            @RequestParam(required = false) Ruangan ruangan,
                            @RequestParam(required = false) AsetJenis asetJenis,
                            @PageableDefault(size = 10) Pageable pageable){
        model.addAttribute("ruangan", ruangan);
        if (StringUtils.hasText(search)){
            model.addAttribute("listAsset", asetDao.findByStatusNotInAndNamaAsetContainingIgnoreCaseOrderByNamaAsetAsc(Arrays.asList(StatusRecord.HAPUS), search, pageable));
        }else if(ruangan != null){
            model.addAttribute("listAsset", asetDao.findByStatusNotInAndRuanganOrderByNamaAset(Arrays.asList(StatusRecord.HAPUS), ruangan, pageable));
        }else if(asetJenis != null){
            model.addAttribute("listAsset", asetDao.findByStatusNotInAndAsetJenisOrderByAsetJenis(Arrays.asList(StatusRecord.HAPUS), asetJenis, pageable));
        } else {
            model.addAttribute("listAsset", asetDao.findByStatusNotInOrderByNamaAsetAsc(Arrays.asList(StatusRecord.HAPUS), pageable));
        }


        return "masters/asset/list";
    }

    @GetMapping("/masters/asset/form")
    public String assetForm(){
        return "masters/asset/form";
    }

    @GetMapping("/update/barcodeNull")
    public String updateBarcodeNull() throws Exception {
        List<Aset> asetList = asetDao.listBarcodeNull();
        for (Aset dataAset : asetList){
            Integer qrCodeSize = 300;


            Integer productNumberLength = 3;
            Integer productQuantity = 1;
            String id = dataAset.getId();
            String productCode = id.substring(0, 6);

            String folderOutput = uploadQrcode + File.separator + productCode;
            new File(folderOutput).mkdirs();
            for (int i = 1; i <= productQuantity; i++) {
                String productNumber = productCode + String.format("%1$" + productNumberLength + "s", i).replace(' ', '1');

                System.out.println("Generate QR Code for product number "+ productNumber);
                BufferedImage qrCode = generateQRcode("https://ga.tazkia.ac.id/qr?code=" +
                        productCode + "/" + productNumber + ".png", qrCodeSize);

                ImageIO.write(qrCode, "png", new File(folderOutput+File.separator
                        +productNumber + ".png"));


                Optional<Aset> aset = asetDao.findById(dataAset.getId());
                Aset updateAset = aset.get();
                log.info("Aset Update :" + updateAset);
                updateAset.setBarcode(productCode + "/" +productNumber + ".png");
                asetDao.save(updateAset);
                log.info("Aset Update :" + updateAset);
            }
        }
        return "redirect:/masters/asset";
    }

    @PostMapping("/asset/updateBarcode")
    public String updateBarcode(Aset aset) throws Exception {
        List<Aset> asetList = asetDao.findByStatus(StatusRecord.AKTIF);
        for (Aset dataAset : asetList) {
            Integer qrCodeSize = 300;


            Integer productNumberLength = 3;
            Integer productQuantity = 1;
            String id = dataAset.getId();
            String productCode = id.substring(0, 6);

            String folderOutput = uploadQrcode + File.separator + productCode;
            new File(folderOutput).mkdirs();




            for (int i = 1; i <= productQuantity; i++) {
                String productNumber = productCode + String.format("%1$" + productNumberLength + "s", i).replace(' ', '1');

                System.out.println("Generate QR Code for product number "+ productNumber);
                BufferedImage qrCode = generateQRcode("https://ga.tazkia.ac.id/qr?code=" +
                        productCode + "/" + productNumber + ".png", qrCodeSize);

                ImageIO.write(qrCode, "png", new File(folderOutput+File.separator
                                +productNumber + ".png"));

                aset.setId(dataAset.getId());
                aset.setNamaAset(dataAset.getNamaAset());
                aset.setAsetJenis(dataAset.getAsetJenis());
                aset.setRuangan(dataAset.getRuangan());
                aset.setMerek(dataAset.getMerek());
                aset.setJumlah(dataAset.getJumlah());
                aset.setHargaPokok(dataAset.getHargaPokok());
                aset.setKondisiAset(dataAset.getKondisiAset());
                aset.setKeterangan(dataAset.getKeterangan());
                aset.setStatus(dataAset.getStatus());
                aset.setBarcode(productCode + "/" +productNumber + ".png");
                aset.setTahunBeli(dataAset.getTahunBeli());
                aset.setStatusPeminjaman(dataAset.getStatusPeminjaman());
                aset.setHargaPinjam(dataAset.getHargaPinjam());
                aset.setFile(dataAset.getFile());
                System.out.println("Success :" + aset.getBarcode());
                asetDao.save(aset);
            }
        }

        return "redirect:/masters/asset";
    }

    @PostMapping("/asset/save")
    public String assetSave(@Valid Aset aset, RedirectAttributes attributes, @RequestParam("image") MultipartFile file)throws Exception{
        if (aset.getRuangan() == null){
            attributes.addFlashAttribute("gagal", "gagal");
        }else{
            if (!file.isEmpty() || file != null) {
                String namaAsli = file.getOriginalFilename();
                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }
                String idFile = UUID.randomUUID().toString();

                String lokasiUpload = uploadAsset + File.separator;
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                file.transferTo(tujuan);





                aset.setFile(idFile + "." + extension);
            }

            Integer qrCodeSize = 300;
            String logoPath = "src/main/resources/static/tampilan/assets/img/tazkia.png";
            Date date = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("ss");
            String productCode = dateFormat.format(date);



            Date date1 = Calendar.getInstance().getTime();
            DateFormat dateFormat1 = new SimpleDateFormat("mm");
            String productionYear = dateFormat1.format(date1);

            Date date2 = Calendar.getInstance().getTime();
            DateFormat dateFormat2 = new SimpleDateFormat("MM");
            String productCode2 = dateFormat2.format(date2);

            Integer productNumberLength = 6;
            Integer productQuantity = 1;

            String folderOutput = uploadQrcode + File.separator + productCode + productCode2;
            new File(folderOutput).mkdirs();

            for (int i = 1; i <= productQuantity; i++) {
                String productNumber = productCode + productionYear + productCode2
                        + String.format("%1$" + productNumberLength + "s", i)
                        .replace(' ', '2');
                System.out.println("Generate QR Code for product number "+productNumber);
                BufferedImage qrCode = generateQRcode("https://ga.tazkia.ac.id/qr?code=" +
                        productCode + productCode2 + "/" + productNumber + ".png", qrCodeSize);
                ImageIO.write(qrCode, "png",
                        new File(folderOutput+File.separator
                                +productNumber + ".png"));
                aset.setBarcode(productCode + productCode2 + "/" +productNumber + ".png");
            }

            BigDecimal hargaAset = aset.getHargaAset();
            BigDecimal pembanding = BigDecimal.valueOf(1000000);
            if (hargaAset.compareTo(pembanding) >= 0){
                aset.setStatusAsset(StatusAset.PERALATAN);
            }else {
                aset.setStatusAsset(StatusAset.PERLENGKAPAN);
            }

            aset.setStatus(StatusRecord.AKTIF);
            asetDao.save(aset);
            attributes.addFlashAttribute("success", "Data saved successfully");
        }
        return "redirect:/masters/asset";

    }



    private static BufferedImage generateQRcode(String barcodeText, Integer size) throws Exception{
        Map<EncodeHintType, ErrorCorrectionLevel> hints = new HashMap<>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        QRCodeWriter barcodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = barcodeWriter.encode(barcodeText, BarcodeFormat.QR_CODE,
                size, size, hints);
        BufferedImage qrCode = MatrixToImageWriter.toBufferedImage(bitMatrix);

        int startingYposition = size-2;

        Graphics2D g = (Graphics2D) qrCode.getGraphics();
        g.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 5));
        Color textColor = Color.BLACK;
        g.setColor(textColor);

        FontMetrics fm = g.getFontMetrics();
        g.drawString(barcodeText, (qrCode.getWidth() / 2) - (fm.stringWidth(barcodeText) / 2), startingYposition);
        return qrCode;
    }

    private static BufferedImage pasangLogoTazkia(BufferedImage qrImage, String logoPath) throws Exception{
        BufferedImage logo = ImageIO.read(new File(logoPath));
        int deltaHeight = qrImage.getHeight() - logo.getHeight();
        int deltaWidth = qrImage.getWidth() - logo.getHeight();

        BufferedImage combined = new BufferedImage(
                qrImage.getHeight(),
                qrImage.getWidth(),
                BufferedImage.TYPE_INT_ARGB
        );

        Graphics2D g = (Graphics2D) combined.getGraphics();
        g.drawImage(qrImage, 0, 0, null);
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        g.drawImage(logo,
                (int) Math.round(deltaWidth/2),
                (int) Math.round(deltaHeight/2),
                null);
        return combined;
    }

    @GetMapping("/masters/gambar/{file}")
    public ResponseEntity<byte[]> fileGambar(@PathVariable Aset file) throws Exception{
        String lokasiFile = uploadAsset + File.separator + file.getFile();
        System.out.println("Test : " + lokasiFile);
        try {
            HttpHeaders headers = new HttpHeaders();
            if (file.getFile().toLowerCase().endsWith("jpeg") || file.getFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(file.getFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(file.getFile().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/masters/aset/{aset}")
    public ResponseEntity<byte[]> detailMobil(@PathVariable Aset aset) throws Exception{
        String lokasiFile = uploadQrcode + File.separator + aset.getBarcode();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (aset.getBarcode().toLowerCase().endsWith("jpeg") || aset.getBarcode().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(aset.getBarcode().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(aset.getBarcode().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/asset/delete")
    public String assetDelete(@RequestParam Aset aset, RedirectAttributes attributes){
        aset.setStatus(StatusRecord.HAPUS);
        asetDao.save(aset);
        attributes.addFlashAttribute("delete", "Data deleted successfully");
        return "redirect:/masters/asset";
    }

    @GetMapping("/asset/edit")
    public String assetBarcode(Model model,@RequestParam(required = false) String id){
        model.addAttribute("asset", asetDao.findById(id));
        return "masters/asset/barcode";
    }

    @PostMapping("/asset/barcode")
    public String assetBarcode(@Valid Aset aset, @RequestParam("image") MultipartFile file,
                               RedirectAttributes attributes) throws Exception {

        // Mengambil aset berdasarkan ID
        Optional<Aset> asetOptional = asetDao.findById(aset.getId());
        if (!asetOptional.isPresent()) {
            attributes.addFlashAttribute("error", "Asset not found");
            return "redirect:/masters/asset";
        }

        Aset asetUpdate = asetOptional.get();

        // Cek apakah file diupload
        if (file != null && !file.isEmpty()) {
            String namaAsli = file.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }

            // Generate ID unik untuk file
            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadAsset + File.separator;

            // Buat direktori jika belum ada
            new File(lokasiUpload).mkdirs();

            // Simpan file ke lokasi tujuan
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);

            // Update file baru
            asetUpdate.setFile(idFile + "." + extension);
            System.out.println("File Baru");
        } else {
            // Gunakan file lama jika file baru tidak diupload
            asetUpdate.setFile(asetOptional.get().getFile());
            System.out.println("File Lama");
        }

        // Update data aset
        asetUpdate.setStatus(StatusRecord.AKTIF);
        asetUpdate.setNamaAset(aset.getNamaAset());
        asetUpdate.setTahunBeli(aset.getTahunBeli());
        asetUpdate.setRuangan(aset.getRuangan());
        asetUpdate.setAsetJenis(aset.getAsetJenis());
        asetUpdate.setMerek(aset.getMerek());
        asetUpdate.setJumlah(aset.getJumlah());
        asetUpdate.setHargaPokok(aset.getHargaPokok());
        asetUpdate.setKondisiAset(aset.getKondisiAset());
        asetUpdate.setKeterangan(aset.getKeterangan());
        asetUpdate.setBarcode(asetOptional.get().getBarcode());

        // Simpan data yang telah diperbarui ke database
        asetDao.save(asetUpdate);

        // Tambah pesan sukses
        attributes.addFlashAttribute("success", "Data saved successfully");

        return "redirect:/masters/asset";
    }


    @PostMapping("/asset/aturHarga")
    public String assetAturHarga(@Valid Aset aset,RedirectAttributes attributes){
        aset.setStatusPeminjaman("BISA_DIPINJAM");
        asetDao.save(aset);
        return "redirect:/masters/asset";
    }

    @GetMapping("/masters/asset/listBarcode")
    public void listBarcode(Model model,@RequestParam Ruangan ruangan,@PageableDefault(size = 1) Pageable pageable){
        model.addAttribute("ruangan", ruangan.getNamaRuangan());
        model.addAttribute("list", asetDao.findByStatusAndRuanganOrderByNamaAset(StatusRecord.AKTIF, ruangan, pageable));
    }

    @GetMapping("/masters/asset/perawatan")
    public void listPerawatan(Model model, @RequestParam Aset aset){
        model.addAttribute("aset", aset);
        model.addAttribute("list", asetPerawatanDao.findByStatusAndAndIdAset(StatusRecord.AKTIF, aset));
    }

    @PostMapping("/masters/asset/perawatan")
    public String addPerawatan(@Valid AsetPerawatan asetPerawatan){
        asetPerawatan.setStatus(StatusRecord.AKTIF);
        asetPerawatanDao.save(asetPerawatan);

        return "redirect:/masters/asset/perawatan?aset=" + asetPerawatan.getIdAset().getId();
    }

    @GetMapping("/masters/asset/perawatan/hapus")
    public String hapusPerawatan(@Valid AsetPerawatan asetPerawatan){
        asetPerawatan.setStatus(StatusRecord.HAPUS);
        asetPerawatanDao.save(asetPerawatan);
        return "redirect:/masters/asset/perawatan?aset=" + asetPerawatan.getIdAset().getId();
    }

    @PostMapping("/updateHargaAset")
    public String updateHargaAset(@Valid UpdateHargaAset updateHargaAset, RedirectAttributes attributes){
        Aset asetUpdate = asetDao.findById(updateHargaAset.getId()).get();
        asetUpdate.setHargaAset(updateHargaAset.getHargaAset());
        asetUpdate.setKategori(updateHargaAset.getKategori());
        asetUpdate.setTanggalBeli(updateHargaAset.getTanggalBeli());
        asetDao.save(asetUpdate);
        attributes.addFlashAttribute("success", "Data saved successfully");
        return "redirect:/masters/asset";
    }

}
