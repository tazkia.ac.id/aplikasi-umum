package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.VendorJenisDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.VendorDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.VendorJenis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class VendorJenisController {
    @Autowired
    private VendorJenisDao vendorJenisDao;

    @GetMapping("/masters/vendorJenis/list")
    public void listVendorList(Model model, @PageableDefault Pageable pageable){
        model.addAttribute("list", vendorJenisDao.findByStatus(StatusRecord.AKTIF, pageable));
    }

    @PostMapping("/masters/vendorJenis/save")
    public String saveVendorJenis(@Valid VendorJenis vendorJenis){
        vendorJenis.setStatus(StatusRecord.AKTIF);
        vendorJenisDao.save(vendorJenis);
        return "redirect:/masters/vendorJenis/list";
    }

    @PostMapping("/masters/vendorJenis/delete")
    public String deleteVendorJenis(@RequestParam VendorJenis vendorJenis){
        vendorJenis.setStatus(StatusRecord.HAPUS);
        vendorJenisDao.save(vendorJenis);
        return "redirect:/masters/vendorJenis/list";
    }
}
