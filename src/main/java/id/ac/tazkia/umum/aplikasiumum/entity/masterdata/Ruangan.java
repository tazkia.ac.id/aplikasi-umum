package id.ac.tazkia.umum.aplikasiumum.entity.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class Ruangan {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_gedung")
    private Gedung idGedung;

    private String kodeRuangan;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_jenis_ruangan")
    private RuanganJenis idJenisRuangan;

    private String namaRuangan;

    private String lantai;

    private String kapasitas;

    private String keterangan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String StatusAktif;

    private String ipMesin;

    private String port;

    private String statusRuangan;
}
