package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Gedung;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface GedungDao extends PagingAndSortingRepository<Gedung, String> {
    Page<Gedung> findByStatus(StatusRecord status, Pageable pageable);
    Iterable<Gedung> findByStatusNotIn(List<StatusRecord> statusRecords);
    List<Gedung> findByStatus(StatusRecord statusRecord);
    Page<Gedung> findByStatusNotInAndNamaGedungContainingIgnoreCaseOrderByNamaGedung(List<StatusRecord> statusRecords,String namaGedung,Pageable pageable);
}
