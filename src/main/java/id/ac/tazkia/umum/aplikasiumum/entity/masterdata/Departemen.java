package id.ac.tazkia.umum.aplikasiumum.entity.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
public class Departemen {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Size(min = 2, max = 50)
    private String kodeDepartemen;

    @NotNull
    @Size(min = 2, max = 50)
    private String namaDepartemen;

    private String keterangan;

    private String statusAktif;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @ManyToOne
    @JoinColumn(name = "id_instansi")
    private Instansi instansi;


}
