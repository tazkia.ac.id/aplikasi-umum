package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.dto.BaseRequestRuangan;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Gedung;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Ruangan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RuanganDao extends PagingAndSortingRepository<Ruangan, String> {
    List<Ruangan> findByStatusAndStatusRuangan(StatusRecord statusRecord, String statusRuangan);
    Iterable<Ruangan> findByStatusNotIn(List<StatusRecord> statusRecords);
    Iterable<Ruangan> findByStatusRuanganAndStatus(String status, StatusRecord statusRecord);

    Page<Ruangan> findByStatusAndIdGedung(StatusRecord statusRecord, Gedung gedung, Pageable pageable);

    Page<Ruangan> findByStatus(StatusRecord statusRecord, Pageable pageable);

    @Query(value = "SELECT id, nama_ruangan as ruangan FROM ruangan where status = 'AKTIF'", nativeQuery = true)
    List<BaseRequestRuangan> getRuangan();

    List<Ruangan> findByStatusOrderByNamaRuanganAsc(StatusRecord statusRecord);
    Page<Ruangan> findByStatusNotInAndKodeRuanganContainingIgnoreCaseOrNamaRuanganContainingIgnoreCaseOrderByNamaRuanganDesc(List<StatusRecord> statusRecords, String kodeRuangan, String namaRuangan, Pageable pageable);
}
