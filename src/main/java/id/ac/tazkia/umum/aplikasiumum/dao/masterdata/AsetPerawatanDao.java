package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Aset;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.AsetPerawatan;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AsetPerawatanDao extends PagingAndSortingRepository<AsetPerawatan, String> {
    List<AsetPerawatan> findByStatusAndAndIdAset(StatusRecord statusRecord, Aset aset);
}
