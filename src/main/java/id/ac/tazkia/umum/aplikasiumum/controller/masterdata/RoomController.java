package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.GedungDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.RuanganDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.RuanganJenisDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Gedung;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Ruangan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.RuanganJenis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Arrays;

@Controller
public class RoomController {

    @Autowired
    private RuanganDao ruanganDao;
    @Autowired
    private GedungDao gedungDao;
    @Autowired
    private RuanganJenisDao ruanganJenisDao;

    @ModelAttribute("gedung")
    public Iterable<Gedung> listGedung() {
        return gedungDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS));
    }

    @ModelAttribute("ruanganJenis")
    public Iterable<RuanganJenis> listRuanganJenis(){
        return ruanganJenisDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS));
    }

    @GetMapping("/masters/room")
    public String listRoom(Model model, @RequestParam(required = false) String search,@PageableDefault(size = 10) Pageable pageable){
        if (StringUtils.hasText(search)){
            model.addAttribute("listRoom", ruanganDao.findByStatusNotInAndKodeRuanganContainingIgnoreCaseOrNamaRuanganContainingIgnoreCaseOrderByNamaRuanganDesc(Arrays.asList(StatusRecord.HAPUS), search, search, pageable));
        }else {
            model.addAttribute("listRoom", ruanganDao.findByStatus(StatusRecord.AKTIF, pageable));
        }
        return "masters/room/list";
    }

    @GetMapping("/masters/room/new")
    public String formRoom(Model model){
        model.addAttribute("room", new Ruangan());
        return "masters/room/form";
    }

    @GetMapping("/masters/room/edit")
    public String roomEdit(Model model, @RequestParam(required = false) String id){
        model.addAttribute("room", ruanganDao.findById(id));
        return "masters/room/form";
    }

    @PostMapping("/masters/room/new")
    public String procesSave(@Valid Ruangan ruangan) {
        ruangan.setStatus(StatusRecord.AKTIF);
        ruangan.setStatusAktif("A");
        ruangan.setIpMesin("-");
        ruangan.setPort("-");
        ruanganDao.save(ruangan);
        return "redirect:../room";
    }

    @PostMapping("/masters/room/delete")
    public String deleteRoom(@RequestParam Ruangan ruangan, RedirectAttributes attributes){
        ruangan.setStatus(StatusRecord.HAPUS);
        ruangan.setStatusAktif("NONAKTIF");
        ruanganDao.save(ruangan);
        attributes.addFlashAttribute("delete", "Delete Data Successfully");
        return "redirect:../room";
    }

}
