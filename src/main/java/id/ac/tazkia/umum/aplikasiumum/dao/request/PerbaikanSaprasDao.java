package id.ac.tazkia.umum.aplikasiumum.dao.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PerbaikanSapras;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PerbaikanSaprasDao extends PagingAndSortingRepository<PerbaikanSapras, String> {

    Page<PerbaikanSapras> findByStatusOrderByTanggalPengajuanDesc(StatusRecord statusRecord, Pageable pageable);
    Page<PerbaikanSapras> findByStatusAndKaryawan(StatusRecord statusRecord, Karyawan karyawan, Pageable pageable);

    List<PerbaikanSapras> findByKaryawanAndStatus(Karyawan karyawan, StatusRecord statusRecord);


}
