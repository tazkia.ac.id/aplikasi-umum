package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.JabatanDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.KaryawanDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.KaryawanJabatanDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Jabatan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.KaryawanJabatan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

@Controller
public class EmployeeController {

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private JabatanDao jabatanDao;

    @ModelAttribute("jabatan")
    public Iterable<Jabatan> listJabatan(){
        return jabatanDao.findByStatusNotInOrderByNamaJabatanAsc(Arrays.asList(StatusRecord.HAPUS));
    }

    @GetMapping("/masters/employee")
    public String listEmployee(Model model, @RequestParam(required = false)String search, @PageableDefault(size = 10)Pageable page){

        if (StringUtils.hasText(search)){
            model.addAttribute("search", search);
            model.addAttribute("listEmployee", karyawanDao.findByStatusAndNikContainingIgnoreCaseOrNamaKaryawanContainingIgnoreCaseOrderByNamaKaryawan(StatusRecord.AKTIF, search, search, page));
        }else {
            model.addAttribute("listEmployee", karyawanDao.findByStatusOrderByNamaKaryawan(StatusRecord.AKTIF, page));
        }

        return "masters/employee/list";
    }

    @GetMapping("/masters/employee/joblist")
    public void employeePosition(Model model, @RequestParam String id){
        model.addAttribute("id", id);
        model.addAttribute("employeePosition", karyawanJabatanDao.listJabatan(id));
    }

    @PostMapping("/masters/employee/jobPosition/delete")
    public String deleleJobPosition(@RequestParam KaryawanJabatan karyawanJabatan){
        String id = karyawanJabatan.getKaryawan().getId();
        karyawanJabatan.setStatusAktif("H");
        karyawanJabatan.setStatus(StatusRecord.HAPUS);
        karyawanJabatanDao.save(karyawanJabatan);

        return "redirect:/masters/employee/joblist?id=" + id;
    }

    @PostMapping("/masters/employee/jobPosition/new")
    public String saveJobPosition(@Valid KaryawanJabatan karyawanJabatan){
        karyawanJabatan.setStatusAktif("A");
        karyawanJabatan.setStatus(StatusRecord.AKTIF);
        karyawanJabatan.setTanggalBerlaku(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        karyawanJabatanDao.save(karyawanJabatan);
        return "redirect:/masters/employee/joblist?id=" + karyawanJabatan.getKaryawan().getId();
    }
    @PostMapping("/masters/employee/delete")
    public String deleteEmployee(@RequestParam Karyawan karyawan){
        karyawan.setStatus(StatusRecord.HAPUS);
        karyawanDao.save(karyawan);
        return "redirect:/masters/employee";
    }

}
