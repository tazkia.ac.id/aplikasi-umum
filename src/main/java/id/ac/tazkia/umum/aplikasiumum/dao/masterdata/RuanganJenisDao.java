package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.RuanganJenis;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RuanganJenisDao extends PagingAndSortingRepository<RuanganJenis, String> {
    Iterable<RuanganJenis> findByStatusNotIn(List<StatusRecord> statusRecords);
}
