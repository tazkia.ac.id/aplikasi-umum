package id.ac.tazkia.umum.aplikasiumum.controller.request;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.*;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanAsetApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanAsetDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.*;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanAset;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanAsetApproval;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PeminjamanKendaraanApproval;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Controller
public class AssetLoanController {

    @Autowired
    private PeminjamanAsetDao peminjamanAsetDao;

    @Autowired
    private AsetJenisDao asetJenisDao;

    @Autowired
    private AsetDao asetDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private PeminjamanAsetApprovalDao peminjamanAsetApprovalDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private JabatanDao jabatanDao;

    @Autowired
    private SettingApprovalDao settingApprovalDao;

    @ModelAttribute("asetCategory")
    public Iterable<AsetJenis> listAsetCategory(){
        return asetJenisDao.findByStatusOrderByJenisAsetAsc(StatusRecord.AKTIF);
    }

    @GetMapping("/assetLoan/list")
    public void listDataAssetLoan(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        model.addAttribute("listData", peminjamanAsetDao.findByStatusAndUserOrderByTanggalPengajuan(StatusRecord.AKTIF, user));
        model.addAttribute("listApproval", peminjamanAsetApprovalDao.listApproval());
    }

    @GetMapping("/assetLoan/form")
    public void formAssetLoan(Model model, @RequestParam(required = false)String jenisAset, @RequestParam(required = false) String kegiatan,
                              @RequestParam(required = false) String tanggalMulaiPeminjaman,
                              @RequestParam(required = false) String tanggalBerakhirPeminjaman) throws ParseException {
        


        if (jenisAset != null){
            model.addAttribute("jenisAset", jenisAset);
            model.addAttribute("kegiatan", kegiatan);
            model.addAttribute("tanggalMulai", tanggalMulaiPeminjaman);
            model.addAttribute("tanggalBerakhir", tanggalBerakhirPeminjaman);

            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            LocalDate mulai = LocalDate.parse(tanggalMulaiPeminjaman);
            LocalDate berakhir = LocalDate.parse(tanggalBerakhirPeminjaman);
            System.out.println("Mulai : " + mulai);
            System.out.println("Berakhir : " + berakhir);

            Period diff = Period.between(mulai, berakhir);
            int hasil = diff.getDays() + 1;

            model.addAttribute("berapaLama", hasil);
            AsetJenis asetJenis = asetJenisDao.findById(jenisAset).get();


            List<PeminjamanAset> peminjamanAsets = peminjamanAsetDao.findByStatusAndTanggalMulaiPeminjamanBetween(StatusRecord.AKTIF, tanggalMulaiPeminjaman, tanggalBerakhirPeminjaman);
            if (peminjamanAsets.isEmpty()){
                model.addAttribute("cek", "kosong");
            }else {
                model.addAttribute("cek", "ada");
                model.addAttribute("cekAset", peminjamanAsetDao.findByStatusAndTanggalMulaiPeminjamanBetween(StatusRecord.AKTIF, tanggalMulaiPeminjaman, tanggalBerakhirPeminjaman));
            }
            model.addAttribute("aset", asetDao.findByStatusAndAsetJenisAndStatusPeminjaman(StatusRecord.AKTIF, asetJenis, "BISA_DIPINJAM"));
        }
    }

    @PostMapping("/assetLoan/save")
    public String saveAssetLoan(PeminjamanAset peminjamanAset, Authentication authentication, RedirectAttributes attributes){
        LocalDate localDate = LocalDate.now(ZoneId.of("UTC+7"));
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        KaryawanJabatan karyawanJabatan = karyawanJabatanDao.findByStatusAndKaryawan(StatusRecord.AKTIF, karyawan);
        Jabatan jabatan = jabatanDao.findById(karyawanJabatan.getJabatan().getId()).get();
        List<SettingApproval> settingApproval = settingApprovalDao.findByStatusAndApproval(StatusRecord.AKTIF, jabatan.getId());
        Integer countTotalApproval = settingApprovalDao.countApproval(jabatan.getId());

        peminjamanAset.setUser(user);
        peminjamanAset.setStatus(StatusRecord.AKTIF);
        peminjamanAset.setTanggalPengajuan(localDate.toString());
        peminjamanAset.setStatusPeminjaman("WAITING");
        peminjamanAset.setStatusPembayaran("BELUM_DIBAYAR");
        peminjamanAset.setNomor(1);
        peminjamanAset.setTotalNomor(countTotalApproval);
        peminjamanAsetDao.save(peminjamanAset);

        for (SettingApproval approval: settingApproval) {
            List<Jabatan> listJabatan = jabatanDao.findByStatusAndId(StatusRecord.AKTIF, approval.getIdJabatan().getId());
            for (Jabatan valueJabatan : listJabatan) {
                System.out.println("test : " + listJabatan);
                List<KaryawanJabatan> karyawanJabatanList = karyawanJabatanDao.findByStatusAndJabatan(StatusRecord.AKTIF, valueJabatan);
                System.out.println("test t : " + karyawanJabatanList);
                for (KaryawanJabatan valueKaryawanJabatan : karyawanJabatanList){
                    PeminjamanAsetApproval peminjamanAsetApproval = new PeminjamanAsetApproval();
                    peminjamanAsetApproval.setPeminjamanAset(peminjamanAset);
                    peminjamanAsetApproval.setIdKaryawan(valueKaryawanJabatan.getKaryawan().getId());
                    peminjamanAsetApproval.setSettingApproval(approval);
                    peminjamanAsetApproval.setStatus(StatusRecord.WAITING);
                    peminjamanAsetApproval.setNomor(approval.getUrutan());
                    peminjamanAsetApprovalDao.save(peminjamanAsetApproval);
                }
            }
        }

        attributes.addFlashAttribute("success", "BERHASIL");
        return "redirect:/assetLoan/list";
    }

    @PostMapping("/assetLoan/batal")
    public String batalAssetLoan(@RequestParam PeminjamanAset peminjamanAset, RedirectAttributes attributes){
        peminjamanAset.setStatus(StatusRecord.HAPUS);
        peminjamanAsetDao.save(peminjamanAset);
        attributes.addFlashAttribute("delete", "BERHASIL");
        return "redirect:/assetLoan/list";
    }

    @GetMapping("/approval/assetLoan/list")
    public void assetLoanApprovalList(Model model){
        model.addAttribute("list", peminjamanAsetDao.findByStatusOrderByTanggalPengajuanDesc(StatusRecord.AKTIF));
    }

    @PostMapping("/approval/assetLoan/approved")
    public String assetLoanApproved(@RequestParam PeminjamanAset peminjamanAset, RedirectAttributes attributes){
        peminjamanAset.setStatusPeminjaman("APPROVED");
        System.out.println("test : " + peminjamanAset.getTanggalPengajuan());
        peminjamanAsetDao.save(peminjamanAset);
        attributes.addFlashAttribute("approved", "Data Approved Successfully");
        return "redirect:/approval/assetLoan/list";
    }

    @PostMapping("/approval/assetLoan/rejeted")
    public String assetLoanRejected(@Valid PeminjamanAset peminjamanAset, RedirectAttributes attributes){
        PeminjamanAset pAset = peminjamanAsetDao.findById(peminjamanAset.getId()).get();
        peminjamanAset.setTanggalPengajuan(pAset.getTanggalPengajuan());
        peminjamanAset.setStatusPeminjaman("REJECTED");
        peminjamanAsetDao.save(peminjamanAset);
        attributes.addFlashAttribute("rejected", "Data Approved Successfully");
        return "redirect:/approval/assetLoan/list";
    }

    @PostMapping("/approval/assetLoan/keterangan")
    public String keteragan(@Valid PeminjamanAset peminjamanAset){
        PeminjamanAset pAset = peminjamanAsetDao.findById(peminjamanAset.getId()).get();
        peminjamanAset.setTanggalPengajuan(pAset.getTanggalPengajuan());
        peminjamanAsetDao.save(peminjamanAset);
        return "redirect:/approval/assetLoan/list";
    }

    @GetMapping("/approval/peminjamanSapras/barang")
    public void barangApproval(Model model){

    }
}
