package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;


import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.DepartemenDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.InstansiDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Departemen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Arrays;

@Controller
public class DepartmentController {

    @Autowired
    private DepartemenDao departemenDao;

    @Autowired
    private InstansiDao instansiDao;

    @GetMapping("/masters/department")
    public String listDepartment(Model model,
                                 @RequestParam(required = false)String search,
                                 @PageableDefault(size = 10)Pageable page){

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listDepartemen", departemenDao.findByStatusNotInAndKodeDepartemenContainingOrNamaDepartemenContainingOrderByKodeDepartemen(Arrays.asList(StatusRecord.HAPUS), search, search, page));
        }else{
            model.addAttribute("listDepartemen", departemenDao.findByStatusOrderByKodeDepartemen(StatusRecord.AKTIF, page));
        }


        return "masters/department/list";
    }
    
    @GetMapping("/masters/department/new")
    public String newDepartemen(Model model){
        model.addAttribute("departemen",new Departemen());
        model.addAttribute("instansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
        return "masters/department/form";
    }

    @GetMapping("/masters/departement/edit")
    public String editDapartemen(Model model, @RequestParam(required = false) String id){
        model.addAttribute("departemen", departemenDao.findById(id));
        model.addAttribute("instansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF));
        return "masters/department/form";
    }

    @PostMapping("/masters/department/new")
    public String prosesDepartment(@Valid Departemen departemen, RedirectAttributes attributes){
        if (departemen.getStatusAktif() == null && departemen.getStatus() == null){
            departemen.setStatusAktif("A");
            departemen.setStatus(StatusRecord.AKTIF);
        }
        departemenDao.save(departemen);
        attributes.addFlashAttribute("success", "Save Data Successfully");
        return "redirect:../department";
    }

    @PostMapping("/masters/department/delete")
    public String deleteDepartment(@RequestParam Departemen departemen, RedirectAttributes attributes){
        departemen.setStatusAktif("H");
        departemen.setStatus(StatusRecord.HAPUS);
        departemenDao.save(departemen);
        attributes.addFlashAttribute("delete", "Delete Data Successfully");
        return "redirect:../department";
    }
}
