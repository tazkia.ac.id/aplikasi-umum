package id.ac.tazkia.umum.aplikasiumum.dto;


import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class UpdateHargaAset {

    private String id;
    private BigDecimal hargaAset;
    private String kategori;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalBeli;
}
