package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Jabatan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.SettingApproval;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SettingApprovalDao extends PagingAndSortingRepository<SettingApproval, String> {
    List<SettingApproval> findByStatus(StatusRecord statusRecord);
    List<SettingApproval> findByStatusAndIdJabatan(StatusRecord statusRecord, Jabatan jabatan);

    List<SettingApproval> findByStatusAndApproval(StatusRecord statusRecord, String id);

    @Query(value = "select count(a.approval) as hasil from setting_approval as a inner join jabatan as b on a.id_jabatan = b.id where a.approval = ?1 and a.status = 'AKTIF'", nativeQuery = true)
    Integer countApproval(String id);
}
