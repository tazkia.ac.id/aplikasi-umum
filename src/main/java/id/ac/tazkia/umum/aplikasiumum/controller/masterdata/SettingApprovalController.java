package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.JabatanDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.SettingApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Jabatan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.SettingApproval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
public class SettingApprovalController {
    @Autowired
    private JabatanDao jabatanDao;

    @Autowired
    private SettingApprovalDao settingApprovalDao;

    @ModelAttribute("position")
    public Iterable<Jabatan> listPosition(){
        return jabatanDao.findByStatusNotInOrderByNamaJabatanAsc(Arrays.asList(StatusRecord.HAPUS));
    }

    @GetMapping("/settingApproval")
    public String listSettingApproval(Model model, @PageableDefault(size = 10) Pageable page, @RequestParam(required = false)String search){
        if (StringUtils.hasText(search)){
            model.addAttribute("position", jabatanDao.findByStatusAndKodeJabatanContainingIgnoreCaseOrNamaJabatanContainingIgnoreCaseOrderByKodeJabatan(StatusRecord.AKTIF, search, search, page));
            model.addAttribute("approval", settingApprovalDao.findByStatus(StatusRecord.AKTIF));
        } else {
            model.addAttribute("position", jabatanDao.findByStatusOrderByKodeJabatan(StatusRecord.AKTIF, page));
            model.addAttribute("approval", settingApprovalDao.findByStatus(StatusRecord.AKTIF));
        }

        return "masters/settingApproval/list";
    }

    @GetMapping("/settingApproval/new")
    public String formSettingApproval(Model model, @RequestParam String id){
        model.addAttribute("countApproval", settingApprovalDao.countApproval(id));
        model.addAttribute("id", id);
        return "masters/settingApproval/form";
    }

    @PostMapping("/settingApproval/save")
    public String saveSetting(@Valid SettingApproval settingApproval){
        settingApproval.setStatus(StatusRecord.AKTIF);

        settingApprovalDao.save(settingApproval);
        return "redirect:/settingApproval";
    }

    @PostMapping("/settingApproval/delete")
    public String deleteSettingApproval(@RequestParam SettingApproval settingApproval){
        settingApproval.setStatus(StatusRecord.HAPUS);
        settingApprovalDao.save(settingApproval);
        String id = settingApproval.getApproval();

        List<SettingApproval> settingApprovals  = settingApprovalDao.findByStatusAndApproval(StatusRecord.AKTIF, id);
        for (SettingApproval data : settingApprovals){
           if (data.getUrutan() == 2){
               SettingApproval settingApproval1 = settingApprovalDao.findById(data.getId()).get();
               settingApproval1.setUrutan(1);
               settingApprovalDao.save(settingApproval1);
           }
        }


        return "redirect:/settingApproval";
    }
}
