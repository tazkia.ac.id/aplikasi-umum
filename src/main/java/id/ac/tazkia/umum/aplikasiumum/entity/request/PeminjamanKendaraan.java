package id.ac.tazkia.umum.aplikasiumum.entity.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Mobil;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class PeminjamanKendaraan {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_karyawan")
    private Karyawan karyawan;

    @ManyToOne
    @JoinColumn(name = "id_mobil")
    private Mobil idMobil;

    private String tujuan;
    private String keperluan;
    private String pengemudi;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalPinjam;

    private LocalDate tanggalPengajuan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

    private String statusPeminjaman;
    private String waktuBerangkat;
    private String waktuPulang;

    private String bulan;
    private String tahun;
    private String statusPembayaran;
    private String bukti;
    private Integer nomor;
    private Integer totalNomor;

    private String tarif;
    private String lampiran;
}
