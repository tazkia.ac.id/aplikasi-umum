package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.AsetJenis;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AsetJenisDao extends PagingAndSortingRepository<AsetJenis, String> {
    Page<AsetJenis> findByStatusNotIn(List<StatusRecord> status, Pageable pageable);
    Iterable<AsetJenis> findByStatusOrderByJenisAsetAsc(StatusRecord statusRecord);
    Iterable<AsetJenis> findByStatusAndStatusPeminjaman(StatusRecord statusRecord, String statusPeminjaman);
}
