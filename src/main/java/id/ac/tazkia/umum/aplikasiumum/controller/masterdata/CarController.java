package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.DepartemenDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.MobilDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Departemen;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Mobil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.util.Arrays;
import java.util.UUID;

@Controller
public class CarController {

    @Autowired
    private MobilDao mobilDao;

    @Value("/tmp/upload/mobil")
    private String uploadMobil;

    @Autowired
    private DepartemenDao departemenDao;

    @ModelAttribute("departemen")
    public Iterable<Departemen> listDepartemen() {
        return departemenDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS));
    }

    @GetMapping("/masters/car/list")
    public void listCar(Model model, @PageableDefault(size = 10)Pageable page){
        model.addAttribute("carList", mobilDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS), page));
    }

    @GetMapping("/masters/car/form")
    public void formCars(Model model){
        model.addAttribute("car", new Mobil());
    }

    @GetMapping("/masters/car/edit")
    public String editCars(Model model, @RequestParam(required = false) String id){
        model.addAttribute("car", mobilDao.findById(id).get());
        return "masters/car/form";
    }

    @PostMapping("/masters/car/new")
    public String saveCars(@Valid Mobil car, @RequestParam("image") MultipartFile file) throws Exception{
        if (!file.isEmpty() || file != null){
            String namaFile = file.getName();
            String jenisFile = file.getContentType();
            String namaAsli = file.getOriginalFilename();
            Long ukuran = file.getSize();

//        memisahkan extensi
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }


            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadMobil + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);

            car.setFile(idFile + "." + extension);
            car.setStatusAktif("A");
            car.setStatus(StatusRecord.AKTIF);

            mobilDao.save(car);

        }else {
            car.setFile(car.getFile());
            car.setStatusAktif("A");
            car.setStatus(StatusRecord.AKTIF);

            mobilDao.save(car);
        }

        return "redirect:/masters/car/list";
    }

//    @PostMapping("/masters/car/update")
//    public String updateCars(@RequestParam Mobil mobil,@RequestParam(required = false) String supir, MultipartFile file) throws Exception{
//            String namaFileAsli = file.getOriginalFilename();
//
//            String extension = "";
//
//            int i = namaFileAsli.lastIndexOf('/');
//            int p = Math.max(namaFileAsli.lastIndexOf('/'), namaFileAsli.lastIndexOf('\\'));
//
//            if (i > p){
//                extension = namaFileAsli.substring(i + 1);
//            }
//
//            String idFile = UUID.randomUUID().toString();
//            String lokasiUpload =  uploadMobil + File.separator + mobil.getMerek();
//            new File(lokasiUpload).mkdirs();
//            File tujuan = new File(lokasiUpload + File.separator + idFile + extension);
//            file.transferTo(tujuan);
//
//            mobil.setFile(idFile + "." + extension);
//
//
//            mobil.setFile(mobil.getFile());
//
//
//        mobil.setStatusAktif("A");
//        mobil.setSupir(supir);
//        mobil.setStatus(StatusRecord.AKTIF);
//
//        mobilDao.save(mobil);
//        return "redirect:../car";
//    }

    @PostMapping("/masters/car/delete")
    public String deleteCar(@RequestParam Mobil mobil){
        mobil.setStatusAktif("NONAKFTIF");
        mobil.setStatus(StatusRecord.HAPUS);

        mobilDao.save(mobil);
        return "redirect:/masters/car/list";
    }

}
