package id.ac.tazkia.umum.aplikasiumum.service;

import id.ac.tazkia.umum.aplikasiumum.dao.config.UserDao;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;


import java.util.logging.Logger;

@Service
public class CurrentUserService {
    @Autowired
    private UserDao userDao;

    public User currentUser(Authentication currentUser){
        OAuth2AuthenticationToken token = (OAuth2AuthenticationToken) currentUser;

        String username = (String) token.getPrincipal().getAttributes().get("email");
        User u = userDao.findByUsername(username);
        return u;

    }
}
