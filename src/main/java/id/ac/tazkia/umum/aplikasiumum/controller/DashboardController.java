package id.ac.tazkia.umum.aplikasiumum.controller;


import id.ac.tazkia.umum.aplikasiumum.dao.config.UserDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.AsetDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.GedungDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.KaryawanDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.RuanganDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.AsetPengadaanApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanKendaraanApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanRuanganApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Gedung;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Ruangan;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DashboardController {

    @Autowired
    private AsetDao asetDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GedungDao gedungDao;

    @Autowired
    private RuanganDao ruanganDao;

    @Autowired
    private AsetPengadaanApprovalDao asetPengadaanApprovalDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PeminjamanRuanganApprovalDao peminjamanRuanganApprovalDao;

    @Autowired
    private PeminjamanKendaraanApprovalDao peminjamanKendaraanApprovalDao;

    @GetMapping("/dashboard")
    public String dashboard(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        model.addAttribute("dashboard", "active");
        model.addAttribute("totalAset", asetDao.countAsetByStatus(StatusRecord.AKTIF));
        model.addAttribute("gedung", gedungDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("aset", asetDao.findByStatus(StatusRecord.AKTIF));

        model.addAttribute("jumlahApprovalRuangan", peminjamanRuanganApprovalDao.countRuanganApproval(karyawan.getId()));
        model.addAttribute("jumlahApprovalKendaraan", peminjamanKendaraanApprovalDao.countApprovalKendaraan(karyawan.getId()));
        model.addAttribute("jumlahApprovalPengadaan", asetPengadaanApprovalDao.countApprovalPengadaan(karyawan));
        return "dashboard";
    }

    @GetMapping("/dashboard/gedung")
    public void gedungDashboard(Model model){
        model.addAttribute("gedung", gedungDao.findByStatus(StatusRecord.AKTIF));
    }

    @GetMapping("/dashboard/ruangan")
    public void ruanganDashboard(Model model, @PageableDefault Pageable pageable){
        model.addAttribute("ruangan", ruanganDao.findByStatus(StatusRecord.AKTIF, pageable));
    }

    @GetMapping("/dashboard/inventaris")
    public void inventarisDashboard(Model model,@PageableDefault Pageable pageable){
        model.addAttribute("ruangan", ruanganDao.findByStatus(StatusRecord.AKTIF, pageable));
        model.addAttribute("aset", asetDao.findByStatus(StatusRecord.AKTIF));
    }

    @GetMapping("/dashboard/detail")
    public void detailGedung(Model model, @RequestParam Gedung gedung, @PageableDefault(size = 20) Pageable pageable){
        model.addAttribute("dataGedung", gedung);
        model.addAttribute("dataRuangan", ruanganDao.findByStatusAndIdGedung(StatusRecord.AKTIF, gedung, pageable));
    }

    @GetMapping("/dashboard/aset")
    public void detailRuangan(Model model, @RequestParam Ruangan ruangan, @PageableDefault(size = 20) Pageable pageable){
        model.addAttribute("ruangan", ruangan);
        model.addAttribute("aset", asetDao.findByStatusAndRuanganOrderByNamaAset(StatusRecord.AKTIF, ruangan, pageable));
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("loginError", true);
        model.addAttribute("login", "active");
        return "login";
    }

    @GetMapping("/")
    public String formAwal(){
        return "redirect:/dashboard";
    }

    @GetMapping("/404")
    public void form404(){
    }



}
