package id.ac.tazkia.umum.aplikasiumum.dao.config;

import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String> {
    User findByUsername(String username);
}
