package id.ac.tazkia.umum.aplikasiumum.controller.request;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.AsetDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.KaryawanDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.RuanganDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.*;
import id.ac.tazkia.umum.aplikasiumum.dto.BaseRequestRuangan;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Aset;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Ruangan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.PerbaikanSapras;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import id.ac.tazkia.umum.aplikasiumum.service.GmailApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller @Slf4j
public class PerbaikanSaprasController {
    @Autowired
    private PeminjamanKendaraanDao peminjamanKendaraanDao;
    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PerbaikanSaprasDao perbaikanSaprasDao;

    @Autowired
    private PeminjamanKendaraanApprovalDao peminjamanKendaraanApprovalDao;

    @Autowired
    private GmailApiService gmailApiService;

    @Autowired
    private MustacheFactory mustacheFactory;

    @Autowired
    private PeminjamanRuanganApprovalDao peminjamanRuanganApprovalDao;

    @Autowired
    private AsetPengadaanApprovalDao asetPengadaanApprovalDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Value("${upload.perbaikan}")
    private String uploadPerbaikan;
    @Autowired
    private RuanganDao ruanganDao;

    @Autowired
    private AsetDao asetDao;

    @GetMapping("/api/ruangan")
    @ResponseBody
    public List<BaseRequestRuangan> listRuangan(){
        List<BaseRequestRuangan> ruangans = ruanganDao.getRuangan();
        return ruangans;
    }

    @GetMapping("/api/barang")
    @ResponseBody
    public List<Aset> asetList(@RequestParam String id){
        Ruangan ruangan = ruanganDao.findById(id).get();
        List<Aset> wilayah = asetDao.findByStatusAndRuangan(StatusRecord.AKTIF, ruangan);
        return wilayah;
    }

    @GetMapping("/api/list")
    @ResponseBody
    public List<PerbaikanSapras> perbaikanSaprasList(Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        List<PerbaikanSapras> list = perbaikanSaprasDao.findByKaryawanAndStatus(karyawan, StatusRecord.AKTIF);
        return list;
    }

    @ModelAttribute("ruangan")
    public Iterable<Ruangan> listRoom(){
        return ruanganDao.findByStatusOrderByNamaRuanganAsc(StatusRecord.AKTIF);
    }

    @GetMapping("/request/perbaikanSapras/list")
    public void perbaikanSaprasList(Model model, @PageableDefault Pageable pageable, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        model.addAttribute("list", perbaikanSaprasDao.findByStatusAndKaryawan(StatusRecord.AKTIF,karyawan ,pageable));
    }

    @GetMapping("/perbaikanSapras/form")
    public String perbaikanSaprasForm(){

        return "request/perbaikanSapras/form";
    }

    @PostMapping("/perbaikanSapras/save")
    public String perbaikanSaprasSave(@Valid PerbaikanSapras perbaikanSapras,
                                      @RequestParam("image") MultipartFile file,
                                      Authentication authentication)throws Exception{
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);

        if (!file.isEmpty() || file != null) {
            String namaAsli = file.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();

            String lokasiUpload = uploadPerbaikan + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);

            perbaikanSapras.setBukti(idFile + "." + extension);
        }

        perbaikanSapras.setKaryawan(karyawan);
        perbaikanSapras.setStatus(StatusRecord.AKTIF);
        perbaikanSapras.setStatusApproved("WAITING");
        perbaikanSapras.setTanggalPengajuan(LocalDate.now());
        perbaikanSaprasDao.save(perbaikanSapras);

        Mustache templateEmail = mustacheFactory.compile("templates/email/infoPerbaikan.html");
        Map<String, String> data = new HashMap<>();
        data.put("namaAset", perbaikanSapras.getAset().getNamaAset());
        data.put("lokasiBarang", perbaikanSapras.getRuangan().getNamaRuangan());
        data.put("namaPengaju", perbaikanSapras.getKaryawan().getNamaKaryawan());
        data.put("tanggalPengajuan", perbaikanSapras.getTanggalPengajuan().toString());
        data.put("keterangan", perbaikanSapras.getKeterangan());
        StringWriter output = new StringWriter();
        templateEmail.execute(output, data);

//        gmailApiService.kirimEmail(
//                "Permohonan Perbaikan Barang",
//                "arifrahmatullah099@gmail.com",
//                "Permohonan Perbaikan Barang oleh " + karyawan.getNamaKaryawan(),
//                output.toString()
//        );

        return "redirect:/request/perbaikanSapras/list";
    }

    @GetMapping("/approval/perbaikanSapras/list")
    public void approvalPerbaikanList(Model model, @PageableDefault(size = 20) Pageable pageable, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        model.addAttribute("jumlahApprovalRuangan", peminjamanRuanganApprovalDao.countRuanganApproval(karyawan.getId()));
        model.addAttribute("jumlahApprovalPengadaan", asetPengadaanApprovalDao.countApprovalPengadaan(karyawan));
        model.addAttribute("jumlahApprovalKendaraan", peminjamanKendaraanApprovalDao.countApprovalKendaraan(karyawan.getId()));
        model.addAttribute("list",perbaikanSaprasDao.findByStatusOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, pageable));
    }

    @GetMapping("/perbaikan/{perbaikanSapras}")
    public ResponseEntity<byte[]> perbaikan(@PathVariable PerbaikanSapras perbaikanSapras) throws Exception{
        String lokasiFile = uploadPerbaikan + File.separator + perbaikanSapras.getBukti();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (perbaikanSapras.getBukti().toLowerCase().endsWith("jpeg") || perbaikanSapras.getBukti().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(perbaikanSapras.getBukti().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(perbaikanSapras.getBukti().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @ResponseBody
    @GetMapping("/api/batal/{id}")
    public PerbaikanSapras batal(@PathVariable PerbaikanSapras id){
        id.setStatus(StatusRecord.HAPUS);
        return perbaikanSaprasDao.save(id);
    }

}
