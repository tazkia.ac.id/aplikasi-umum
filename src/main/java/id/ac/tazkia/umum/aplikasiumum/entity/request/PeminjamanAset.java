package id.ac.tazkia.umum.aplikasiumum.entity.request;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Aset;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Data
@Entity
public class PeminjamanAset {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    @ManyToOne
    @JoinColumn(name = "id_aset")
    private Aset aset;

    private String tanggalMulaiPeminjaman;

    private String tanggalBerakhirPeminjaman;

    private String hari;

    private String tanggalPengajuan;

    private String keterangan;

    private String kegiatan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String statusPeminjaman;

    private String keteranganDitolak;

    private String organisasi;

    private String tagihan;

    private String buktiPembayaran;

    private String statusPembayaran;

    private String kondisi;

    private String deskripsi;

    private Integer nomor;

    private Integer totalNomor;

}
