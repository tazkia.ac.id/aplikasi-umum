package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Kampus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Arrays;
import java.util.List;

public interface KampusDao extends PagingAndSortingRepository<Kampus, String> {
    Page<Kampus> findByStatusNotInAndNamaKampusContainingIgnoreCaseOrderByNamaKampus(List<StatusRecord> asList, String namaKampus, Pageable pageable);
    Page<Kampus> findByStatusOrderByNamaKampusDesc(StatusRecord statusRecord, Pageable pageable);
    List<Kampus> findByStatus(StatusRecord statusRecord);
}
