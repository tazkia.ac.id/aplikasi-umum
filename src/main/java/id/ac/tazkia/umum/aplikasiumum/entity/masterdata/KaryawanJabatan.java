package id.ac.tazkia.umum.aplikasiumum.entity.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class KaryawanJabatan {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_karyawan")
    private Karyawan karyawan;

    @ManyToOne
    @JoinColumn(name = "id_jabatan")
    private Jabatan jabatan;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate mulaiBerlaku;

    private String keterangan;

    private String statusAktif;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String tanggalBerlaku;


}
