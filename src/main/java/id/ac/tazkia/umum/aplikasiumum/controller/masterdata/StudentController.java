package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StudentController {

    @GetMapping("/masters/student")
    public String listStudent(){


        return "masters/student/list";
    }
}
