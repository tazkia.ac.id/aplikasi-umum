package id.ac.tazkia.umum.aplikasiumum.entity.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusAset;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
public class Aset {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String namaAset;

    @ManyToOne
    @JoinColumn(name = "id_jenis_aset")
    private AsetJenis asetJenis;

    @ManyToOne
    @JoinColumn(name = "id_ruangan")
    private Ruangan ruangan;

    private String merek;

    private String jumlah;

    private String hargaPokok;

    private String kondisiAset;

    private String keterangan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;


    private String barcode;

    private String tahunBeli;

    private String statusPeminjaman;

    private String hargaPinjam;

    private String file;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalBeli;

    @Enumerated(EnumType.STRING)
    private StatusAset statusAsset;


    private BigDecimal hargaAset;

    private String kategori;
}
