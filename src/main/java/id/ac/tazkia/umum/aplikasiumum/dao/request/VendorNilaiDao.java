package id.ac.tazkia.umum.aplikasiumum.dao.request;

import id.ac.tazkia.umum.aplikasiumum.entity.request.VendorNilai;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface VendorNilaiDao extends PagingAndSortingRepository<VendorNilai, String> {
    @Query(value = "SELECT id_vendor, sum(nilai / 3) FROM vendor_nilai group by id_vendor", nativeQuery = true)
    List<Object[]> listNilai();
}
