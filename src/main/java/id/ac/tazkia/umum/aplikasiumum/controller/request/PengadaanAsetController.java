package id.ac.tazkia.umum.aplikasiumum.controller.request;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.*;
import id.ac.tazkia.umum.aplikasiumum.dao.request.AsetPengadaanApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanKendaraanApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PeminjamanRuanganApprovalDao;
import id.ac.tazkia.umum.aplikasiumum.dao.request.PengadaanAsetDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.*;
import id.ac.tazkia.umum.aplikasiumum.entity.request.AsetPengadaan;
import id.ac.tazkia.umum.aplikasiumum.entity.request.AsetPengadaanApproval;
import id.ac.tazkia.umum.aplikasiumum.service.CurrentUserService;
import id.ac.tazkia.umum.aplikasiumum.service.GmailApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;

@Controller @Slf4j
public class PengadaanAsetController {

    @Value("${upload.bukti}")
    private String uploadBukti;

    @Value("${upload.lampiran}")
    private String uploadLampiran;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PengadaanAsetDao pengadaanAsetDao;

    @Autowired
    private AsetJenisDao asetJenisDao;

    @Autowired
    private RuanganDao ruanganDao;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private KaryawanJabatanDao karyawanJabatanDao;

    @Autowired
    private JabatanDao jabatanDao;

    @Autowired
    private GmailApiService gmailApiService;

    @Autowired
    private MustacheFactory mustacheFactory;

    @Autowired
    private AsetDao asetDao;

    @Autowired
    private AsetPengadaanApprovalDao asetPengadaanApprovalDao;

    @Autowired
    private SettingApprovalDao settingApprovalDao;

    @Autowired
    private PeminjamanKendaraanApprovalDao peminjamanKendaraanApprovalDao;

    @Autowired
    private PeminjamanRuanganApprovalDao peminjamanRuanganApprovalDao;

    @ModelAttribute("roomList")
    public Iterable<Ruangan> listRoom(){
        return ruanganDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS));
    }

    @ModelAttribute("asetCategory")
    public Iterable<AsetJenis> listAsetCategory(){
        return asetJenisDao.findByStatusOrderByJenisAsetAsc(StatusRecord.AKTIF);
    }

    @GetMapping("/request/assetProcurement")
    public String listAssetProcurement(Model model, @PageableDefault(size = 10) Pageable pageable, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        model.addAttribute("listAssetProcurement", pengadaanAsetDao
                            .findByStatusAndUserOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, user, pageable));
        model.addAttribute("listApproval", asetPengadaanApprovalDao.listApproved());
        return "request/assetProcurement/list";
    }

    @GetMapping("/request/assetProcurement/form")
    public String formAssetProcurement(){
        return "request/assetProcurement/form";
    }

    @PostMapping("/request/assetProcurement/save")
    public String saveAssetProcurement(@Valid AsetPengadaan asetPengadaan, RedirectAttributes attributes,
                                       Authentication authentication,
                                       @RequestParam("image") MultipartFile file) throws Exception{

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        KaryawanJabatan karyawanJabatan = karyawanJabatanDao.findByStatusAndKaryawan(StatusRecord.AKTIF, karyawan);
        Jabatan jabatan = jabatanDao.findById(karyawanJabatan.getJabatan().getId()).get();
        List<SettingApproval> settingApproval = settingApprovalDao.findByStatusAndApproval(StatusRecord.AKTIF, jabatan.getId());
        Integer countTotalApproval = settingApprovalDao.countApproval(jabatan.getId());

        if (!file.isEmpty() || file != null){
            String namaAsli = file.getOriginalFilename();
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }
            String idFile = UUID.randomUUID().toString();

            String lokasiUpload = uploadLampiran + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);

            asetPengadaan.setLampiran(idFile + "." + extension);
            asetPengadaan.setUser(user);
            asetPengadaan.setStatus(StatusRecord.AKTIF);
            asetPengadaan.setStatusApproved("WAITING");
            asetPengadaan.setStatusPembayaran("BELUM_DIAJUKAN");
            asetPengadaan.setStatusProses("BELUM_TERSIMPAN_DIDATA_ASET");
            asetPengadaan.setTanggalPengajuan(LocalDate.now());
            asetPengadaan.setNomor(1);
            asetPengadaan.setTotalNomor(countTotalApproval);
            pengadaanAsetDao.save(asetPengadaan);

            for (SettingApproval approval: settingApproval){

                List<Jabatan> listJabatan = jabatanDao.findByStatusAndId(StatusRecord.AKTIF, approval.getIdJabatan().getId());
                for (Jabatan valueJabatan: listJabatan){
                    System.out.println("test : " + listJabatan);
                    List<KaryawanJabatan> karyawanJabatanList = karyawanJabatanDao.findByStatusAndJabatan(StatusRecord.AKTIF, valueJabatan);
                    System.out.println("test t : " + karyawanJabatanList);
                    for (KaryawanJabatan valueKaryawanJabatan : karyawanJabatanList){
                        AsetPengadaanApproval asetPengadaanApproval = new AsetPengadaanApproval();
                        asetPengadaanApproval.setAsetPengadaan(asetPengadaan);
                        asetPengadaanApproval.setApproval(approval);
                        asetPengadaanApproval.setNomor(approval.getUrutan());
                        asetPengadaanApproval.setIdKaryawan(valueKaryawanJabatan.getKaryawan().getId());
                        asetPengadaanApproval.setStatus(StatusRecord.WAITING);

                        System.out.println("email : " + valueKaryawanJabatan.getKaryawan().getEmail());
                        Mustache templateEmail = mustacheFactory.compile("templates/email/emailPengadaan.html");
                        Map<String, String> data = new HashMap<>();
                        data.put("namaPengaju", karyawan.getNamaKaryawan());
                        data.put("namaAset", asetPengadaan.getNamaAset());
                        data.put("jumlah", asetPengadaan.getJumlah());
                        data.put("keterangan", asetPengadaan.getKegunaan());
                        data.put("spek", asetPengadaan.getSpekAset());
                        data.put("satuan", asetPengadaan.getSatuan());
                        data.put("lampiran", asetPengadaan.getId());
                        StringWriter output = new StringWriter();
                        templateEmail.execute(output, data);

                        gmailApiService.kirimEmail(
                                karyawan.getEmail(),
                                valueKaryawanJabatan.getKaryawan().getEmail(),
                                "Permohonan Pengadaan Barang oleh " + karyawan.getNamaKaryawan(),
                                output.toString()
                        );
                        asetPengadaanApprovalDao.save(asetPengadaanApproval);
                    }
                }

            }
            attributes.addFlashAttribute("success", "Save Data Successfully");
            return "redirect:/request/assetProcurement";
        }else {
            attributes.addFlashAttribute("tidakBerhasil", "Pengajuan Gagal File Lampiran Kosong ...");
            return "redirect:/request/assetProcurement";
        }


    }


    @PostMapping("/request/assetProcurement/cancel")
    public String cancelAssetProcurement(@RequestParam AsetPengadaan asetPengadaan, RedirectAttributes attributes){
        asetPengadaan.setStatus(StatusRecord.HAPUS);
        pengadaanAsetDao.save(asetPengadaan);
        attributes.addAttribute("delete", "Delete data successful...");
        return "redirect:/request/assetProcurement";
    }

    @GetMapping("/approval/assetProcurement")
    public String listApprovalAssetProcurement(Model model, @PageableDefault(size = 10) Pageable pageable){
        model.addAttribute("listApproval", pengadaanAsetDao.findByStatusOrderByTanggalPengajuanDesc(StatusRecord.AKTIF, pageable));
        return "approval/assetProcurement/list";
    }

    @PostMapping("/approapproval/assetProcurement/approved")
    public String approvedAssetProcurement(@RequestParam AsetPengadaan asetPengadaan){
        asetPengadaan.setTanggalApproved(LocalDate.now());
        asetPengadaan.setStatusApproved("APPROVED");
        pengadaanAsetDao.save(asetPengadaan);
        return "redirect:/approval/assetProcurement";
    }

    @PostMapping("/approval/assetProcurement/rejected")
    public String rejectedAssetProcurement(AsetPengadaan asetPengadaan){
        AsetPengadaan asetPengadaan1 = pengadaanAsetDao.findById(asetPengadaan.getId()).get();

        asetPengadaan1.setTanggalPengajuan(LocalDate.now());
        asetPengadaan1.setStatusApproved("REJECTED");
        asetPengadaan1.setKeteranganDitolak(asetPengadaan.getKeteranganDitolak());
        pengadaanAsetDao.save(asetPengadaan1);
        return "redirect:/approval/pengadaanAset/list";
    }

    @GetMapping("/approval/assetProcurement/listApproved")
    public String listApprovedAssetProcurement(Model model, @PageableDefault(size = 10)Pageable pageable, @RequestParam(required = false) String status){
        model.addAttribute("status", status);
        if (StringUtils.hasText(status)){
            model.addAttribute("listApproved", pengadaanAsetDao.findByStatusAndStatusApprovedAndStatusPembayaranOrderByTanggalApprovedDesc(StatusRecord.AKTIF, "APPROVED", status, pageable));
        }else {
            model.addAttribute("listApproved", pengadaanAsetDao.findByStatusAndStatusApprovedOrderByTanggalApprovedDesc(StatusRecord.AKTIF, "APPROVED", pageable));
        }
        return "approval/assetProcurement/listApproved";
    }

    @GetMapping("/approval/assetProcurement/payment/list")
    public String paymentList(Model model, @PageableDefault(size = 10)Pageable pageable, @RequestParam(required = false) String status){
        model.addAttribute("status", status);
        if (StringUtils.hasText(status)){
            model.addAttribute("listApproved", pengadaanAsetDao.findByStatusAndStatusApprovedAndStatusPembayaranOrderByTanggalApprovedDesc(StatusRecord.AKTIF, "APPROVED", status, pageable));
        }else {
            model.addAttribute("listApproved", pengadaanAsetDao.findByStatusAndStatusApprovedOrderByTanggalApprovedDesc(StatusRecord.AKTIF, "APPROVED", pageable));
        }
        return "approval/assetProcurement/paymentList";
    }

    @PostMapping("/approval/assetProcurement/payment")
    public String paymentAssetProcurement(@RequestParam AsetPengadaan asetPengadaan, MultipartFile file) throws Exception {
        if (!file.isEmpty() || file != null){
            String namaFile = file.getName();
            String namaAsliFile = file.getOriginalFilename();
            String extension = "";

            int i = namaAsliFile.lastIndexOf('.');
            int p = Math.max(namaAsliFile.lastIndexOf('/'), namaAsliFile.lastIndexOf('\\'));
            if (i > p){
                extension = namaAsliFile.substring(i + 1);
            }

            String idFile = UUID.randomUUID().toString();
            String lokasiUpload = uploadBukti + File.separator;
            new File(lokasiUpload).mkdirs();
            File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
            asetPengadaan.setBuktiPembayaran(idFile + "." + extension);
        }else {
            asetPengadaan.setBuktiPembayaran(asetPengadaan.getBuktiPembayaran());
        }

        asetPengadaan.setStatusPembayaran("SUDAH_DIBAYAR");
        pengadaanAsetDao.save(asetPengadaan);
        return "redirect:/approval/assetProcurement/listApproved";
    }

    @GetMapping("/approval/assetProcurement/assetRecording")
    public String recordingAssetProcurement(Model model, @RequestParam String id){
        model.addAttribute("assetRecording", pengadaanAsetDao.findById(id));
        return "approval/assetProcurement/assetRecording";
    }

    @PostMapping("/approval/assetProcurement/assetRecording/save")
    public String saveAssetRecording(@Valid Aset aset, @RequestParam String id){
        aset.setStatus(StatusRecord.AKTIF);
        asetDao.save(aset);
        AsetPengadaan asetPengadaan = pengadaanAsetDao.findById(id).get();
        asetPengadaan.setStatusProses("SUDAH_TERSIMPAN_DIDATA_ASET");
        pengadaanAsetDao.save(asetPengadaan);
        return "redirect:/approval/assetProcurement/listApproved";
    }

    @PostMapping("/approval/assetProcurement/pangajuanDana")
    public String pengajuanDanaAssetProcurement(@Valid AsetPengadaan asetPengadaan){
        String id = asetPengadaan.getId();
        AsetPengadaan asetPengadaan1 = pengadaanAsetDao.findById(id).get();
        asetPengadaan.setTanggalPengajuan(asetPengadaan.getTanggalPengajuan());
        asetPengadaan.setTanggalApproved(asetPengadaan.getTanggalApproved());
        asetPengadaan.setTanggalPengajuan(asetPengadaan1.getTanggalPengajuan());
        asetPengadaan.setTanggalApproved(asetPengadaan1.getTanggalApproved());
        asetPengadaan.setStatusPembayaran("PENGAJUAN_DANA");
        if (asetPengadaan.getTotalHarga() <= 100000000){
            asetPengadaan.setProsesPengadaan("LANGSUNG");
        }else {
            asetPengadaan.setProsesPengadaan("LELANG");
        }
        pengadaanAsetDao.save(asetPengadaan);
        return "redirect:/approval/assetProcurement/listApproved";
    }

    @GetMapping("/approval/assetProcurement/listPayment")
    public String listPayment(Model model, @RequestParam(required = false) String status, @PageableDefault Pageable pageable){
        model.addAttribute("status", status);
        model.addAttribute("listPayment", pengadaanAsetDao.findByStatusAndStatusPembayaranOrderByTanggalApprovedDesc(StatusRecord.AKTIF, status, pageable));
        return "approval/assetProcurement/paymentApproved";
    }

    @PostMapping("/approval/assetProcurement/approvedPayment")
    public String DanaAssetProcurement(@RequestParam AsetPengadaan asetPengadaan){
        asetPengadaan.setStatusPembayaran("BELUM_DIBAYAR");
        pengadaanAsetDao.save(asetPengadaan);
        return "redirect:/approval/assetProcurement/listPayment";
    }

    @GetMapping("/approval/pengadaanAset/list")
    public void getApproval(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByUser(user);
        model.addAttribute("listApproval", asetPengadaanApprovalDao.listApproval(karyawan));
        model.addAttribute("jumlahApproval", asetPengadaanApprovalDao.countApprovalPengadaan(karyawan));
        Integer jumlahApprovalKendaraan = peminjamanKendaraanApprovalDao.countApprovalKendaraan(karyawan.getId());
        if (jumlahApprovalKendaraan.equals(null)){
            model.addAttribute("jumlahApprovalKendaraan", 0);
        }else{
            model.addAttribute("jumlahApprovalKendaraan", jumlahApprovalKendaraan);
        }
        model.addAttribute("jumlahApprovalRuangan", peminjamanRuanganApprovalDao.countRuanganApproval(karyawan.getId()));
    }

    @GetMapping("/masters/pengadaan/{lampiran}/")
    public ResponseEntity<byte[]> detailMobil(@PathVariable String lampiran) throws Exception{
        AsetPengadaan asetPengadaan = pengadaanAsetDao.findById(lampiran).get();
        String lokasiFile = uploadLampiran + File.separator + asetPengadaan.getLampiran();
        log.debug("Test : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (asetPengadaan.getLampiran().toLowerCase().endsWith("jpeg") || asetPengadaan.getLampiran().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if(asetPengadaan.getLampiran().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(asetPengadaan.getLampiran().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @PostMapping("/approval/pengadaanAset/save")
    public String approvalPengadaanAset(@RequestParam AsetPengadaan asetPengadaan, @RequestParam AsetPengadaanApproval asetPengadaanApproval){
        Integer jumlahApproved = asetPengadaan.getNomor() + 1;

        if (jumlahApproved > asetPengadaan.getTotalNomor()){
            asetPengadaan.setStatusPembayaran("BELUM_DIBAYAR");
            asetPengadaan.setStatusApproved("APPROVED");
            pengadaanAsetDao.save(asetPengadaan);
        }else {
            asetPengadaan.setStatusPembayaran("BELUM_DIBAYAR");
            asetPengadaan.setNomor(jumlahApproved);
            pengadaanAsetDao.save(asetPengadaan);
        }

        asetPengadaanApproval.setTanggalApprove(LocalDate.now());
        asetPengadaanApproval.setStatus(StatusRecord.APPROVED);
        asetPengadaanApprovalDao.save(asetPengadaanApproval);

        return "redirect:/approval/pengadaanAset/list";
    }

    @GetMapping("/approval/pengadaanAset/sudahBayar")
    public String sudahBayar(@RequestParam AsetPengadaan asetPengadaan){
        asetPengadaan.setStatusPembayaran("SUDAH_DIBAYAR");
        pengadaanAsetDao.save(asetPengadaan);
        return "redirect:/approval/assetProcurement/listApproved";
    }

    @PostMapping("/approval/pengadaanAset/penyerahan")
    public String penyerahanBarang(@Valid AsetPengadaan asetPengadaan){
        AsetPengadaan pengadaan = pengadaanAsetDao.findById(asetPengadaan.getId()).get();

        asetPengadaan.setNamaAset(pengadaan.getNamaAset());
        asetPengadaan.setUser(pengadaan.getUser());
        asetPengadaan.setSpekAset(pengadaan.getSpekAset());
        asetPengadaan.setJumlah(pengadaan.getJumlah());
        asetPengadaan.setSatuan(pengadaan.getSatuan());
        asetPengadaan.setKegunaan(pengadaan.getKegunaan());
        asetPengadaan.setStatus(pengadaan.getStatus());
        asetPengadaan.setStatusApproved(pengadaan.getStatusApproved());
        asetPengadaan.setTanggalPengajuan(pengadaan.getTanggalPengajuan());
        asetPengadaan.setStatusPembayaran(pengadaan.getStatusPembayaran());
        asetPengadaan.setProsesPengadaan(pengadaan.getProsesPengadaan());
        asetPengadaan.setNomor(pengadaan.getNomor());
        asetPengadaan.setTotalNomor(pengadaan.getTotalNomor());

        pengadaanAsetDao.save(asetPengadaan);
        return "redirect:/approval/assetProcurement/listApproved";
    }
}
