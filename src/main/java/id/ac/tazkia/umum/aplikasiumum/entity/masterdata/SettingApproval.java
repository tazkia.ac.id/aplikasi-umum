package id.ac.tazkia.umum.aplikasiumum.entity.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
public class SettingApproval {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String id;

    @OneToOne
    @JoinColumn(name = "id_jabatan")
    private Jabatan idJabatan;

    private Integer urutan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;


    private String approval;
}
